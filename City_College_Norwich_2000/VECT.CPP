/* simple implementation of vector algebra   K. SAADI */

#include<iostream.h>
#include<conio.h>

class vector
{
 public:
   vector(int n=1):itsDim(n){}; // default constructor;
   //~vector(); // destructor
  // public accessors
  float get(int i){ return itsVal[i-1];}
  void set(int i, float x){ itsVal[i-1]=x;}
  friend int dim(vector a);
  //overloading operator +
  friend vector operator+(vector& a,vector& b);
 private:
  int itsDim;
  float itsVal[10];
};

int dim(vector a)
{                    // friend function
 return a.itsDim;
}
vector operator+(vector& a, vector& b)
{
 vector c;
 for(int i=0;i<=a.itsDim-1;i++)
  c[i]=a.itsVal[i]+b.itsVal[i];
 return c;
}





void main()
{
 vector v(3);
 v.set(1,12.5);
 v.set(2,34.5);
 v.set(3,35.78);
 cout<<v.get(2)<<endl;
 getch();
}

