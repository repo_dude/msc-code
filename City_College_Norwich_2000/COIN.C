#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<conio.h>
#include<stdlib.h>

typedef enum{FALSE,TRUE} bool;

void getint(char input[],int len)
{
 bool write;
 char in;
 int n=0,end;
 gotoxy(10,10);printf("___");
 gotoxy(10,10);
 do
 { in=getch();
   
   if(in=='\b' && n>=1)
	{ putch(in);
	  putch(' ');
	  putch('\b');
	  input[--n]=' '; }
	if(isdigit(in) && n<=len-1)
	{
	  write=FALSE;
	  switch(n)
	  {
		case 0 : if( in=='1' || in=='2' || in=='5') write=TRUE;break;
		case 1 : if( in=='0') write=TRUE;break;
		case 2 : if( in=='0' && input[0]=='1') write=TRUE;
	  }
	  if (write)
	  {
		input[n++]=in;
		putch(in);
	  }
	}
  } while(in!='\r');
}

int main()
{
 char val[3];
 int s=0,k;
 clrscr();
 do {
   strcpy(val,"0");
   getint(val,3);
   s+=atoi(val);
   gotoxy(0,0);gotoxy(12,12); printf("\n %d ",s);
 } while((k=atoi(val))!=0);
 return 0;
}

 
