#include<stdio.h>
#include<conio.h>

int main()
{
  const int n=4;
  float a[4][4]={{1,2,2,2},
		 {2,1,2,2},
		 {2,2,1,2},
		 {2,2,2,1}};
  float b[]={7,7,7,7};
  float x[4];
  float d,s;
  int i,j,k;

  //clrscr();
					 /* triangularizatio */
  for(k=0;k<=n-2;k++)
   for(i=k+1;i<=n-1;i++)
   {
    d=a[i][k]/a[k][k];
	b[i] -= d*b[k];
	for(j=0;j<=n-1;j++) a[i][j] -= d*a[k][j];
   }
   for(i=0;i<=n-1;i++)
   {
	for(j=0;j<=n-1;j++) printf("  %10.6f",a[i][j]);
	printf("  %10.6f \n",b[i]);
   }                 /*******************/
   for(i=n-1;i>=0;i--)
   {
	for (s=0.0,j=i+1;j<=n-1;j++) s+=a[i][j]*x[j];
	x[i]=(b[i]-s)/a[i][i];
   }
   printf(" Solution: \n");
   for(i=0;i<=n-1;i++) printf("  %10.6f",x[i]);

   getch();
   return 0;
}

