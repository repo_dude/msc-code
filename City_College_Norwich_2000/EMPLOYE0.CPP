/*************************************************************************

 Employee ver 1.0

 this code declares a class called Employee with the private data members
 Age,yearsOfService,Salary.And then declares two objects person1,person2
 to set and display their data members.

 K.SAADI
***************************************************************************/


#include<iostream.h>
#include<conio.h>

class Employee
  {
 public:
   // *  Public accessors *
   void setAge(int anAge);
   void setYearsOfService(int aPeriode);
   void setSalary(float money);

   int getAge() const;
   int getYearsOfService() const;
   float getSalary() const;

 private:
   int Age,yearsOfService;
   float Salary;
  };

void Employee::setAge(int anAge)
{
 Age=anAge;
}

void Employee::setYearsOfService(int aPeriode)
{
 yearsOfService=aPeriode;
}

void Employee::setSalary(float money)
{
 Salary=money;
}

int Employee::getAge() const
{
 return Age;
}

int Employee::getYearsOfService() const
{
 return yearsOfService;
}

float Employee::getSalary() const
{
 return Salary;
}

int main()
{
 void print(Employee);//function prototype to print Employee object

 Employee person1,person2;

 clrscr();
 // initializing person1
 person1.setAge(28);
 person1.setYearsOfService(8);
 person1.setSalary(10000);
 // initializing person2
 person2.setAge(40);
 person2.setYearsOfService(20);
 person2.setSalary(30000);
 // printing out
 print(person1);
 print(person2);
 getch();
 return 0;
}

void print(Employee person)
{
 cout<<"\n Age              : "<<person.getAge();
 cout<<"\n Years of Service : "<<person.getYearsOfService();
 cout<<"\n Salary           : "<<person.getSalary()<<"\n";
}

