
/*
  Calculating eigen"Latent" values and eigen vectors with iteration methode
  using deflation.
  In this case all e.v are real due to the symmetry of a.         K. Saadi
*/

#include<stdio.h>
#include<conio.h>
#include<math.h>
#include"linear.h"
//#include"vector.h"

void main()
{
  matrix a(4,4);
  vector x(4),x2(4);
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

  a.set(1,1,1);a.set(1,2,1);a.set(1,3,1);a.set(1,4,1);
  a.set(2,1,1);a.set(2,2,2);a.set(2,3,1);a.set(2,4,1);
  a.set(3,1,1);a.set(3,2,1);a.set(3,3,3);a.set(3,4,1);
  a.set(4,1,1);a.set(4,2,1);a.set(4,3,1);a.set(4,4,4);

  x.set(1,0.5);x.set(2,0.5);x.set(3,0.5);x.set(4,0.5);
/*--------------------------------------------------------*/

  double d=0,d2,s;
  int k=0,l=0;
  clrscr();
  cout<<" The matrix : "<<"\n\n"<<a<<endl;
  cout<<" iters:    EigenValue:  EigenVector: "<<"\n";
  cout<<">-----<    >----------< >----------------------------------------<\n\n";   
  while(l<4)
  {
   l++;
   d=99999;
   k=0;
   do
   {
	 k++;
	 x2=a*x;
	 d2=d;
	 d=mod(x2)/mod(x);
	 x=x2;
	 x.normalize();
   } while( fabs(d2-d)>=1e-12 );
   printf("    %3d    %4.6f   ",k,d);
   cout<<x<<endl;
   /* -----Deflation-------- */
   a=a-d*(x|x);
   
 }  
 getch();
}

