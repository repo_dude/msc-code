#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<ctype.h>
#include<dos.h>

void layout();
void getint(char input[],int len);
int getintxy(int x,int y,int width);

int main()
{
/*----------------------- Initialisation --------------------------------*/ 
 char *station[24]={
     "Parkway Station", "Blount Roudabout","Gaumont","Commercial Union",
     "Bank Street","Butlocks Heath","Tescos","Town Centre","Eastern Arcade",
     "Mill Street","Cathedral","Coach Station","Odeon","Shirley Shops",
     "Netley Abbey","Market Junction","Industrial Estate","Grove Lane",
     "The Windmill","Havelock Square","Mayfair Theatre","Leighton Avenue",
     "Woolston Road","Windover Roundabout"};
/*-------------------------------------------------------------------------*/ 
 int i;
 char next_ticket='Y';
 int nbr_stat;
 int ad_cost,
	 ch_cost,    /* minimum charge is 15 */
	 oap_cost=15;
 int totalm=0,totalp=0;
 int Total_of_personnes=0,
	 Total_of_money=0;
 int current_stop;

 clrscr();
 layout();
 /* entering the BusStop nbr from which this programm runs */
 while((i=getintxy(21,3,2))<1 || i>24); 
 current_stop=i;
 gotoxy(45,3);
 printf("%s",station[i-1]);
 while(next_ticket!='N')
 {
   /* entering the destination busStop nbr */
   while((i=getintxy(21,7,2))<1 || i>24);/* validation for the bus stop nbr*/
   gotoxy(45,7);
   printf("%s",station[i-1]);
   nbr_stat=i-current_stop;
   if(nbr_stat>0)
   {
	 totalm=totalp=0;
	 i=getintxy(21,9,2);
	 if(i!=0)             /* i=nbr of adults */
	 {
	   ad_cost= 15+nbr_stat*10;
	   totalp+=i;
	   totalm+=i*ad_cost;
	   gotoxy(21,10);printf("%4dp",ad_cost);
	   gotoxy(21,11);printf("%4dp",i*ad_cost);
	 }
	 i=getintxy(45,9,2);
	 if(i!=0)             /* i= nbr of children */
	 {
	   ch_cost= 15+nbr_stat*6;
	   totalp+=i;
	   totalm+=i*ch_cost;
	   gotoxy(45,10);printf("%4dp",ch_cost);
	   gotoxy(45,11);printf("%4dp",i*ch_cost);
	 }
	 i=getintxy(65,9,2);  /* i = nbr of OAPs */
	 if(i!=0)
	 {
	   totalp+=i;
	   totalm+=i*oap_cost;
	   gotoxy(65,10);printf("%4dp",oap_cost);
	   gotoxy(65,11);printf("%4dp",i*oap_cost);
	 }
	 gotoxy(21,13);printf("%4d",totalp);
	 gotoxy(45,13);printf("�%4.2f",(float)totalm/100);
	 Total_of_personnes+=totalp;
	 Total_of_money+=totalm;
	 delay(5000);
   }
   clrscr();
   printf("\n\n\t\t Do you want another ticket? <Y>es or <N>o");
   while( (next_ticket=_toupper(getch()))!='Y' && next_ticket!='N');
   if(next_ticket=='Y') 
   {
	 clrscr();
	 layout();
	 gotoxy(21,3);printf("%d",current_stop);
	 gotoxy(45,3);printf("%s",station[current_stop-1]);
   }
  }
  clrscr();
  printf("\n\n\n\t\t Total of personnes :%3d",Total_of_personnes);
  printf("\n\t\t Total of money : �%4.2f",(float)(Total_of_money)/100);
  getch();
  return 0;
}

void layout()
{
 printf("   BOARDED AT \n\n");
 printf("   Stop No_________:....   Stop name_______:........................\n\n");
 printf("   TRAVELLING TO \n\n");
 printf("   Stop No_________:....   Stop name_______:........................\n\n");
 printf("   No.of Adults____:....   No.of Children__:....   No.of OAPs__:....\n");
 printf("   Adult fare______:....   Child fare______:....   OAP fare____:....\n");
 printf("   Cost adults_____:....   Cost children___:....   Cost of OAPs:....\n\n");
 printf("   Total No persons:....   Total fare paid :.... \n");
}

void getint(char input[],int len)
{
 char in;
 int n=0;
 do
 { in=getch();
   
   if(in=='\b' && n>=1)
	{ 
	  putch('\b');
	  putch('.');
	  putch('\b');
	  n--;
	}
	if(isdigit(in) && n<=len-1)
	{
	  input[n++]=in;
	  putch(in);
	}
  } while(in!='\r' && n!='\t');
  input[n]='\0';
}

int getintxy(int x,int y,int width)
{
 char input[4];
 gotoxy(x,y);printf("....");
 gotoxy(x,y);
 getint(input,width);
 return atoi(input);
}










 
		     






