/*
	Tutorial 2: handling files
	Author    : Kamel Saadi 
*/

/*----------------------------------------------------------------------

  this application is about reading a file containing some american  
  cities with the corresponding latitude and longitude.
    
------------------------------------------------------------------------*/
  

import java.io.*;
import java.util.*;

import java.awt.*;
import java.awt.event.*;

public class TrivialApplication extends Frame implements ActionListener,
	      WindowListener {

	private Button         loadFile;
	private Button         nextRecord;
	private TextField      fileField;
	private TextField      cityField;
	private TextField      latitudeField;
	private TextField      longitudeField;
	private TextField      fileStatus;
	private FileDialog     getFileName;
	private BufferedReader inFile;
	private int            recordNumber=0;

	public static void main(String[] args) {
		TrivialApplication f = new TrivialApplication();
		f.setSize(200,200);
		f.display();
		f.setVisible(true);
	}
	public void display() {
	       setLayout( new GridLayout(5,2,10,10) );

	       loadFile = new Button("Load File");
	       fileField = new TextField(20);

	       Label city = new Label(     "  City       :");
	       cityField = new TextField(20);

	       Label latitude = new Label( "  Latitude   :");
	       latitudeField = new TextField(20);

	       Label longitude = new Label("  Longitude  :");
	       longitudeField = new TextField(20);

	       nextRecord = new Button(" Next");
	       fileStatus = new TextField(20);

	       add(loadFile);
	       add(fileField);

	       add(city);
	       add(cityField);

	       add(latitude);
	       add(latitudeField);

	       add(longitude);
	       add(longitudeField);

	       add(nextRecord);
	       add(fileStatus);
	      
	       loadFile.addActionListener(this);
	       nextRecord.addActionListener(this);
	       this.addWindowListener(this);
	}
	public void actionPerformed(ActionEvent evt)
	{
	    String fileName;
	    if(evt.getSource()==loadFile) {
	      getFileName = new FileDialog(this,"File Name",FileDialog.LOAD);
	      getFileName.show();
	      fileName = getFileName.getFile();
	      fileField.setText(fileName);
	      fileStatus.setText(" reading...");
	      try{
		inFile = new BufferedReader( new FileReader(fileName) );
	      }
	      catch(IOException e)  {
		   System.err.println(e.toString());
	      }
	      readRecord();
	    }
	    if(evt.getSource()==nextRecord) {
		readRecord();
	    }
	}
	private void readRecord() {
	    String line;
	    StringTokenizer data;
	    try {
		if ( (line = inFile.readLine())!=null ) {
		   data = new StringTokenizer(line,",");
		   while(data.hasMoreTokens()) {
		     cityField.setText(data.nextToken());
		     latitudeField.setText(data.nextToken());
		     longitudeField.setText(data.nextToken());
		   }
		   recordNumber++;
		   fileStatus.setText(Integer.toString(recordNumber));
		}
		else {
		 nextRecord.setEnabled(false);
		 inFile.close();
		 fileStatus.setText("end of file");
		}
	    }
	    catch(IOException e)  {
		   System.err.println(e.toString());
	    }
	}

	public void windowClosing(WindowEvent event){
		System.exit(0);
	}
	public void windowOpened(WindowEvent event) {
	}
	public void windowClosed(WindowEvent event) {
	}
	public void windowIconified(WindowEvent event) {
	}
	public void windowDeiconified(WindowEvent event) {
	}
	public void windowActivated(WindowEvent event) {
	}
	public void windowDeactivated(WindowEvent event) {
	}

}
