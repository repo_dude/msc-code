class Display extends Frame implements ActionListener,
              WindowListener {
        
        private Label     serve;
        private TextField serveField;
        private Button    up;
        public  TextArea  displayQueue; 
        private Label     customer;
        private Label     name;
        private TextField nameField;  
        private Label     number; 
        private TextField numberField;
        private Button    insert;
        private Label     status;
        private Label     aName;
        private TextField aNameField; 
        private Button    check;
        private Label     position;
        private TextField pos;
        private Panel     p1,p2,p3;     
        private Queue     theQueue;      
                
     
        public Display( Queue aQueue){
              theQueue = aQueue;             
                                
              setBackground(Color.gray);
              setLayout(new GridLayout(1,3,40,10));
              
              serve        =   new Label("Serving");
              serveField   =   new TextField(20);
              up           =   new Button("^");
              displayQueue =   new TextArea(10,20); 
              
              p1 = new Panel();                         
              
              p1.add(serve);
              p1.add(serveField);
              p1.add(up);
              p1.add(displayQueue);
              
              customer     =   new Label("Customer");
              name         =   new Label("Name :");
              nameField    =   new TextField(20); 
              number       =   new Label("Account# :"); 
              numberField  =   new TextField(12);
              insert       =   new Button("<-- Add");
              
              p2 = new Panel();
              
              p2.add(customer);                                      
              p2.add(name);
              p2.add(nameField);
              p2.add(number);
              p2.add(numberField);
              p2.add(insert);
              
              
              status       =   new Label("Status");
              aName        =   new Label("Name :");
              aNameField   =   new TextField(20); 
              check        =   new Button("Check");
              position     =   new Label("Position");
              pos          =   new TextField(3);
              
              p3 = new Panel();
              
              p3.add(status);
              p3.add(aName);
              p3.add(aNameField);
              p3.add(check);
              p3.add(position);
              p3.add(pos);
              
              add(p1);
              add(p2);
              add(p3);
                            
              up.addActionListener(this);
              check.addActionListener(this);
              insert.addActionListener(this);                     
              this.addWindowListener(this);
              
        }
        public void actionPerformed(ActionEvent evt)
        {
          if (evt.getSource()==up && theQueue.head!=null){                       
             serveField.setText(theQueue.head.data.getName());
             theQueue.remove();         
          }
          if (evt.getSource()==insert){
             if (nameField!=null && numberField!=null) {
                theQueue.add(nameField.getText(),numberField.getText());
                nameField.setText("");
                numberField.setText("");
             }
          }
          if (evt.getSource()==check)
            pos.setText(Integer.toString(theQueue.getPosition(aNameField.getText())));      
          
          repaint(); 
                
        }
        public void windowClosing(WindowEvent event){
                System.exit(0);
        }        
        public void windowOpened(WindowEvent event) {
        }
        public void windowClosed(WindowEvent event) {
        } 
        public void windowIconified(WindowEvent event) {
        }
        public void windowDeiconified(WindowEvent event) {
        }
        public void windowActivated(WindowEvent event) {
        }
        public void windowDeactivated(WindowEvent event) {
        }
        public void paint(Graphics g)
        {
          displayQueue.setText(theQueue.content());
        }

}          
