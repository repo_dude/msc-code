class Queue {
     
       public Node head;
       public Node tail;
       public int  count;
       
       public Queue(){
       
          head=null;
          tail=null;
          count=0;
       }
           
       public void add(String name,String numb){
          Node newNode = new Node(new Customer(name,numb));
          if (count==0){
            head=newNode;
            tail=head;
          }
          else tail.next=newNode;
          tail=newNode;
          count++;
       }
       
       public void remove(){
          if(head!=null){
           head=head.next;
           count--;
          }
       }
       
       public String content(){
          Node temp=head;
          String report="";
          while(temp!=null){
             report+=temp.data.getName()+"\t"+temp.data.getNumber()+"\n";              
             temp=temp.next;
          }        
          return report; 
       }    
       
       public int getPosition(String theName){
          Node current=head;
          boolean found=false;
          int pos=0;
          while(current!=null && !found){
              if ( theName.toUpperCase().equals(current.data.getName().toUpperCase()) )
               found=true;
              pos++;
              current=current.next;
          }
          if (!found) pos=0;
          return pos;
       }       
} 


class Customer {
       
       private String name;
       private String number;
       
       public Customer(String aName,String aNumber){ 
          name   = aName;
          number = aNumber;
       }   
       
       public String getName(){
          return name;
       }
       public String getNumber(){
          return number;
       }              
        
}

class Node {
       public Customer data;
       public Node     next=null;

       public Node( Customer newCustomer){
          data=newCustomer;
       }      
}       