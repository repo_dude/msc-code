/*
	Trivial application that displays a string - 4/96 PNL
*/
import java.awt.*;
//import java.awt.Graphics;
import java.io.*;
import java.util.*;

public class TrivialApplication {

	private static Frame f = new Frame();
	
	public static void main(String args[]) throws IOException {
		FileDialog fd = new FileDialog(f,"Sample");
		fd.show();
		String pathName = fd.getDirectory();
		System.out.println(pathName);
		FileReader input = new FileReader("pathName");
	    BufferedReader inFile = new BufferedReader(input);
	    String line;
	    while((line=inFile.readLine())!=null)
	     System.out.println(line);
		inFile.close();
		fd.dispose();

	}
}
