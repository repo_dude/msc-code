/*
 * Multiple Knapsack problem - Scatter Search 
 * 
 *    MAX (SUM{pi*xi})             i=1,..,n
 *
 *    subject to SUM{wij*xj}<= bi  j=1,..,n; i=1,..,m
 *
 *    wij,bi,pi >=0
 * 
 * K. Saadi 
 *
 */
 
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

class Scatter extends Frame implements ActionListener,WindowListener
{   

    public   TextArea         text; 
    public   boolean          display=false;
    public   Button           loadExample;
    public   Button           saveResult;
    public   TextField        loadName;
    public   TextField        saveName;
    public   Button           randomExample;
    private  PrintWriter      outFile;
   	private  FileDialog       getFileName;
	private  BufferedReader   inFile;
    private  Label            Iters;
    private  TextField        Iterations; 
     
    public   int              maxIters;    
    public   KnapSacks        knps;
    
    public   int[][]          bestSolutions;
     
 	public Scatter()
	{
		setLayout(new FlowLayout());
		setBackground(Color.lightGray);

		loadExample = new Button("Open example");
		add(loadExample);

		loadName = new TextField(20);
		add(loadName);
		
		saveResult = new Button("Save Results");
		add(saveResult);
		
		saveName = new TextField(20);
		add(saveName);
		
		randomExample = new Button("Random Example");
		add(randomExample);

		Iters = new Label("  Max Iterations :");
		Iterations = new TextField(5);
		add(Iters);
		add(Iterations);

		loadExample.addActionListener(this);
		saveResult.addActionListener(this);
		randomExample.addActionListener(this);
		 
		text = new TextArea(20,70);
		
        text.setFont(new Font("Dialog",Font.PLAIN,14));
 		add(text);
 		this.addWindowListener(this);
	}
           
	public void getData(){
		maxIters = Integer.parseInt(Iterations.getText());
	}	
		
	public void actionPerformed(ActionEvent evt){
		if(evt.getSource()==randomExample){
	        int n0 = 6 + (int)(Math.random()*10);
	        int m0 = 2 + (int)(Math.random()*6);
	        knps = new KnapSacks(n0,m0);
	        bestSolutions = new int[10][n0];
	        knps.setProblem();
	        text.appendText(knps.toString());
            getData();
            scatterSearch();  
	    }
	    if(evt.getSource()==saveResult){
            try {
                 outFile = new PrintWriter(
                                new FileWriter(saveName.getText()),true);
                 outFile.println(text.getText() );
                 outFile.close();
                 saveName.setText("");
            }
            catch (IOException e) {
                 System.err.println("File Error: "+ e.toString());
                 System.exit(1);
            }
 	    }
        if(evt.getSource()==loadExample) {
          String fileName; 	
	      String directory;
	      getFileName = new FileDialog(this,"File Name",FileDialog.LOAD);
	      getFileName.show();
	      directory = getFileName.getDirectory();
	      fileName = getFileName.getFile();
	      loadName.setText(fileName);
	      try{
		     inFile = new BufferedReader( new FileReader(directory+fileName) );
	      }
	      catch(IOException e){
		    System.err.println(e.toString());
	      }
	      
	      String content="";;
          String line;
	      StringTokenizer data;
	      try {
		     while ((line = inFile.readLine())!=null )content +=line;
		     data = new StringTokenizer(content,",");
		     int n0 = Integer.parseInt(data.nextToken());
		     int m0 = Integer.parseInt(data.nextToken());
		     bestSolutions = new int[10][n0];
		     knps = new KnapSacks(n0,m0);
		     knps.readFromString(content);
		     //knps.generateInitialSol();
		  }catch(IOException e)  {
		     System.err.println(e.toString());
	      }
	      text.appendText(knps.toString());
          getData();
          scatterSearch();   
	  }
 	     repaint();	
	}
	
	public void scatterSearch(){
		double dx;
		int[] optimal = new int[knps.n];
		int max=0;
		int popSize = 25;
		knps.generateInitialSol();
		for(int i=0;i<maxIters;i++){
		  int solNumb=0;
		  for(int k=0;k<popSize;k++){
		    knps.generateNeighbourSol();
		  	dx =knps.costNewX-knps.costX;
		    if (dx>0){ 
			     knps.affect(knps.x,knps.newX);// x <-- NewX
			     knps.costX = knps.costNewX;
			     // choosing the first 10 best solutions
			     if(solNumb<bestSolutions.length){
			       for(int l=0;l<bestSolutions[0].length;l++)
			     	 bestSolutions[solNumb][l]=knps.x[l];
		           solNumb++;
		         }
		         // storing the best known solution
      	         if (knps.costX>max){
                   max = knps.costX;
                   knps.affect(optimal,knps.x);
                 }
		    }
		  }
		  // combining solutions
		  int [] comb = new int[knps.n];
		  for(int t=0; t<knps.n; t++){
		      int h=0;
		      while((h<bestSolutions.length)&&(h<solNumb)){
		  	    comb[t]+=bestSolutions[h][t];
		  	    h++;
		  	  }
		  }	
		  // normalising
		  for(int t=0;t<comb.length;t++){
		  	if (comb[t]>=5) comb[t]=1;
		  	else comb[t]=0;
		  }	   
		  text.appendText(knps.printSol());
		  knps.affect(knps.x,comb);
		  if(knps.feasible(comb)) knps.setCost();
		  else knps.costX=0;       
		}
		knps.affect(knps.x,optimal);
		knps.costX=max;
		text.appendText("\n BEST SOLUTION:");
		text.appendText(knps.printSol());
	}		

	public static void main(String args[]) {
		System.out.println("Starting Scatter...");
		Scatter scatter = new Scatter();
		scatter.setSize(650, 450);
		scatter.setTitle("Multiple KnapSack problem - Scatter Search");
		scatter.setVisible(true);
  	} 
  	
    public void windowClosing(WindowEvent event){System.exit(0);}
    public void windowOpened(WindowEvent event) {}
    public void windowClosed(WindowEvent event) {}
    public void windowIconified(WindowEvent event) {}
    public void windowDeiconified(WindowEvent event) {}
    public void windowActivated(WindowEvent event) {}
    public void windowDeactivated(WindowEvent event) {}
    
}            	
  	 



class KnapSacks{
	public int         n; // the number of objects xi
	public int         m; // the number of knapsacks
	public int[]       p; // profits pi
	public int[][]     w; // weights wij; j=1,...,n ; i=1,...,m
	public int[]       c; // knapsack capacities cj
	public int[]       x; // curent solution        
	public int[]       newX;// new solution
	public int         costX;// the cost(Objective function) of the current solution
	public int         costNewX;// the cost of the new solution
	public int         pos;
	
	public KnapSacks( int n0, int m0){
		n    = n0;
		m    = m0;
		p    = new int[n]; 
	    x    = new int[n]; 
		newX = new int[n];
		c    = new int[m];
		w    = new int[m][n];
		costNewX=costX=0;
	}

	public void generateInitialSol(){ 
		 do {
		     for (int i=0;i<n;i++){
		       if(Math.random()<0.5) x[i]=1; // generating an initial
		                      else   x[i]=0; //  random feasible solution
             }
         }while (!feasible(x));
         costX=0;
		 for(int i=0;i<n;i++) costX+=p[i]*x[i];
    }
    
    public void setCost(){
    	costX=0;
    	for(int i=0;i<n;i++) costX+=p[i]*x[i];
    }	
    	
	public void affect(int[] x1,int[] x2){
		for(int i=0;i<n;i++) x1[i]=x2[i];
	}	
			
	public void generateNeighbourSol(){ 
           int sign;
           do{
           	  pos = (int)(Math.random()*n);
           	  affect(newX,x);
           	  if (newX[pos]==0) {
           	  	  newX[pos]=1;
           	  	  sign = 1;
           	  }	  
           	  else { 
           	      newX[pos]=0;
           	      sign=-1;
           	  }    
           }while (!feasible(newX));
           costNewX =costX + sign*p[pos];
    }           	   
		
	public void setProblem(){
		for(int i=0;i<n;i++) p[i]=random(10); //  generating profits pi 
		
		for(int i=0;i<m;i++){	
	   		c[i] = 10+random(20);// generating capacities ci
			for(int j=0;j<n;j++)
				w[i][j]=random(10); // generating weights wij
		}
	}			
	
	private int random(int N){
		int rand = (int)(Math.random()*N)+1;
		return rand;
	}
	
	public boolean feasible(int[] x){
		boolean can = true;
		int  sum;
		int  i=0; 
		while (can && i<m){	
	   		 sum =0;
	   		 for(int j=0;j<n;j++)
		        sum+=w[i][j]*x[j];
		     can = (sum<=c[i]);
		     if (can==false) break;
		     i++;    
		}
		return can;
	}	     

	public String toString(){
		String report="\n\nPROBLEM DETAILS:\n\n";
		report+="Object size n = "+n;
		report+="\n\nNumber of Knapsacks = "+m;
		report+="\n\nPROFITS:\n";
		 for(int i=0;i<n;i++) report+=p[i]+"\t";
		report+="\n\nWEIGHTS\n";
		int i=0;
		while (i<m){
		  report+="\n";
		  for(int j=0;j<n;j++) report+=w[i][j]+"\t"; 
		  i++;
		} 
		report +="\n\nCAPACITIES\n";
		for(int j=0;j<m;j++)report+="\n"+c[j];
		report+="\n\nSOLUTION\n\n";
			return report;
	}
	public String printSol(){
		String	report="\n";
		for (int i=0;i<n;i++)
			report+=x[i]+"\t";
		report += "\t"+costX;	
		return report;	
	}
	
	// the format is
	// n , m,
	// p[i],
	// c[i],
	// w[1][j],
	// w[2][j],
	public void readFromString(String example){
		StringTokenizer data;
		data = new StringTokenizer(example,",");
		while(data.hasMoreTokens()) {
		       n = Integer.parseInt(data.nextToken());
		       m = Integer.parseInt(data.nextToken());
		       for(int i=0;i<n;i++)
		         p[i] = Integer.parseInt(data.nextToken());
		       for(int i=0;i<m;i++)
		         c[i]= Integer.parseInt(data.nextToken()); 
		       for(int i=0;i<m;i++)
		        for(int j=0;j<n;j++)
		          w[i][j]=Integer.parseInt(data.nextToken()); 
		}
	}
			 	 
}

	
	
	
			