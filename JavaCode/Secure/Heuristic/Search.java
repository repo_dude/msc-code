/*
 * @(#)Heuristic.java 1.0 01/05/18
 *
 * You can modify the template of this file in the
 * directory ..\JCreator\Templates\Template_2\Project_Name.java
 *
 * You can also create your own project template by making a new
 * folder in the directory ..\JCreator\Template\. Use the other
 * templates as examples.
 *
 */
import methods.*;
import java.awt.*;
import java.applet.*;
//import java.awt.*;
import java.awt.event.*;
//import java.io.*;
//import java.util.*;



public class Search extends Applet implements ActionListener
{
	private Button sim;
	private Button tabu;
	private Button scatter;
	
	public void init()
	{
		sim  = new Button("Simulated Annealing");
		add(sim);
		tabu = new Button("Tabu Search");
		add(tabu);
		scatter = new Button("Scatter Search");
		add(scatter);
		sim.addActionListener(this);
		tabu.addActionListener(this);
		scatter.addActionListener(this);

	}
	public void actionPerformed(ActionEvent evt){
		if (evt.getSource()==sim){
			SimAnn sa = new SimAnn();
			sa.show();
			sa.close();
		}
		repaint();
	}		

	public void paint(Graphics g)
	{
		//g.drawString("Welcome to Java!!", 50, 60 );
	}

}
