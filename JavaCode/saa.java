// {$R saa.JFM}

import java.awt.*;

// Class saa
public class saa extends Frame
{
    final int MenuBarHeight = 0;
    boolean fForm_Create;

    // Component Declaration
    // End of Component Declaration

    // Constructor
    public saa()
    {
        // Frame Initialization
        setForeground(Color.black);
        setBackground(Color.lightGray);
        setFont(new Font("Dialog",Font.BOLD,12));
        setTitle("noname01");
        setLayout(null);
        // End of Frame Initialization

        // Component Initialization
        // End of Component Initialization

        // Add()s
        // End of Add()s

        fForm_Create = true;
    }

    public void InitialPositionSet()
    {
        // InitialPositionSet()
        reshape(139,238,400,300);
        // End of InitialPositionSet()

        fForm_Create = false;
    }

    public boolean handleEvent(Event evt)
    {
        // handleEvent()
        if (evt.id == Event.WINDOW_DESTROY && evt.target == this) saa_WindowDestroy(evt.target);
        // End of handleEvent()

        return super.handleEvent(evt);
    }

    public void paint(Graphics g)
    {
        // paint()
        // End of paint()
        if (fForm_Create) InitialPositionSet();
    }

    // Event Handling Routines
    public void saa_WindowDestroy(Object target)
    {
        hide();
    }

    // End of Event Handling Routines

} // End of Class saa