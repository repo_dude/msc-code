//------------------------------------------------------------------------//
// Tutorial 4 : creating an maintaining a CD Collection
// Author     : Kamel Saadi
//------------------------------------------------------------------------//     

import java.text.*;
public class TrivialApplication {

    public static void main (String[] args){
       ListD music = new ListD();
       music.sortAdd("Jill Scot","Who is it",12.99,20);
       music.sortAdd("Phil Collins","Feet Mountain",10.99,12);
       music.sortAdd("Phil Collins","False Mountain",12.99,12);
       music.sortAdd("Phil Collins","Far away",12.99,10); 
       music.sortAdd("Bigur Ros","Agaetis Byrjun",14.99,10);
       music.sortAdd("Nitin Sawhney","Broken Skin",10.99,14);
       System.out.println(" Creating an ordered list:\n");
       System.out.print(music);
       
       System.out.println("\n Insertion at position 5  :\n");
       music.insertAt(5,"Michael Jackson","Dangerous",13.99,12);
       System.out.print(music);
       
       System.out.println("\n Removing from position 4 :\n");
       music.remove(4);
       System.out.print(music);
    }
}


class ListD{

    private CDNode head,browse;
    private CDNode last;
    public  int    length=0;

    public ListD() {
    }
      // in order to maintain a list we always need to know it's head
      // head is a reference to the first item in the list
     
    
    /*----- the case of unordered list -------*/
    
    public void addCD(String artist,String title,double cost, int tracks){
	     CDNode pos,lastPos;
	     CD     aCD = new CD(artist,title, cost, tracks);
	     CDNode temp= new CDNode(aCD);

	     if (length==0) head=temp;	        	      
	     else browse.next=temp;	       
	     length++;
	     browse=temp;	      	    
    }
    
    /*------ Adding items to the list in alphabetical order -----------*/
    
    public void sortAdd(String artist,String title,double cost, int tracks){
         CDNode pos,lastPos;
	     CD     aCD = new CD(artist,title, cost, tracks);
	     CDNode temp= new CDNode(aCD);
	     int n=3;// the value 3 doesn't mean anything
	     if (length==0){ head=temp;length++;}
	     else{
	        pos=head;
	        lastPos=pos;
	        /*---- comparing names -------*/
	        while(pos!=null && (n=pos.theCD.artist.compareTo(artist))<0){
	            lastPos=pos;
	            pos=pos.next;
	        }
	        /*---- comparing titles ------*/
	        if(pos!=null && n==0){
	         while(pos!=null && (n=pos.theCD.title.compareTo(title))<=0){  
	            lastPos=pos;
	            pos=pos.next;
	         }   
            }	        
	        if (pos==null) insertTail(temp);
	        else {
	           if(pos==head) insertHead(temp);
	           else insert(lastPos,temp);
	        }	           
	     }
	}                	        	      	     	       	    	     
              
    /*------ inserting  newNode at position n from the head ---*/   
    
    public void insertAt(int n,String artist,String title,double cost,int tracks)
    {
         CD     aCD = new CD(artist,title, cost, tracks);
	     CDNode newNode= new CDNode(aCD);
         CDNode pos=head;
         
         if(n==1) insertHead(newNode); 
         if (n>1 && n<=length)
         {
          for(int i=2;i<n;i++) pos=pos.next;// looking for postion n-1
          insert(pos,newNode);
         }
         if(n==length+1) insertTail(newNode);
         if(n<1 || n>length+1)
          System.out.println("Impossible insertion");
      }
                                   
    /*---------- Insering nodeB after nodeA ------------------*/
    
    public void insert(CDNode nodeA,CDNode nodeB) {
	    nodeB.next=nodeA.next;
		nodeA.next =nodeB;
		if (nodeB==head) head=nodeA;
		length++;
    }
    /*---------- inserting newHead at the top of the list -----*/
    
    public void insertHead(CDNode newHead){
        newHead.next=head;
        head=newHead;
        length++;
    }
    
    /*---------- inserting newTail at the end of the list -----*/
    
    public void insertTail(CDNode newTail){
        CDNode pos=head;
        for(int i=1;i<length;i++) pos=pos.next;
        pos.next=newTail;
        length++;
    }      
        
    /*-------- removing node from positiond n ----------------*/
    
    public void remove(int n){
    
        CDNode pos=head;
        if(n==1) { 
            head=pos.next;
            length--;
        }    
        if(n>1 && n<=length){
          for(int i=2;i<n;i++) pos=pos.next;// looking for position n-1
          pos.next=pos.next.next;
          length--;
        }
        if(n<1 || n>length)
         System.out.println("Impossible removal");
    }      
    
    /*---------------------- reading the list ----------------*/
    public String toString(){
  	    String report="";
	    browse=head;
	    while(browse!=null){
	      report += browse.theCD.toString()+"\n";
	      browse=browse.next;
	    }
	    return report;
    }

    /*------------------------------------------------------------------*/
    /*--------------------- inner classes ------------------------------*/
    /*------------------------------------------------------------------*/

    public class CDNode {
	    public CD     theCD;
	    public CDNode next;

	    public CDNode(CD newCD){
		  theCD = newCD;
		  next  = null;
	    }
    }

    public class CD {

	    private String  artist;
	    private String  title;
	    private double  cost;
	    private int     tracks;

	    public CD(String xArtist,String xTitle,double xCost, int xTracks)
	    {
		artist = xArtist;
		title  = xTitle;
		cost   = xCost;
		tracks = xTracks;
	    }
        public String toString() {
		String out;
		out= artist+"\t"+title+"\t�"+cost+"\t"+tracks;
		return out;
	    }

    }
}



