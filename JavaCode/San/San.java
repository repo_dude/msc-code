
import java.awt.*;

// Class Boo
public class Boo extends Frame
{
    final int MenuBarHeight = 0;

    // Component Declaration
    public TextField TextChrom;
    public TextArea text;
    public TextField TextPm;
    public TextField TextPc;
    public TextField TextPop;
    public Label Population;
    public Label Label1;
    public Label Label2;
    public Label Label3;
    public Button Proceed;
    // End of Component Declaration

    // Constructor
    public Boo()
    {
        // Frame Initialization
        setForeground(Color.black);
        setBackground(Color.lightGray);
        setFont(new Font("Dialog",Font.BOLD,12));
        setTitle("noname01");
        setLayout(null);
        // End of Frame Initialization

        // Component Initialization
        TextChrom = new TextField("");
        TextChrom.setForeground(Color.black);
        TextChrom.setBackground(Color.white);
        TextChrom.setFont(new Font("Dialog",Font.BOLD,11));
        text = new TextArea("");
        text.setForeground(Color.green);
        text.setBackground(Color.black);
        text.setFont(new Font("Dialog",Font.PLAIN,12));
        TextPm = new TextField("");
        TextPm.setForeground(Color.black);
        TextPm.setBackground(Color.white);
        TextPm.setFont(new Font("Dialog",Font.BOLD,11));
        TextPc = new TextField("");
        TextPc.setForeground(Color.black);
        TextPc.setBackground(Color.white);
        TextPc.setFont(new Font("Dialog",Font.BOLD,11));
        TextPop = new TextField("");
        TextPop.setForeground(Color.black);
        TextPop.setBackground(Color.white);
        TextPop.setFont(new Font("Dialog",Font.BOLD,11));
        Population = new Label("Label1",Label.LEFT);
        Population.setFont(new Font("Dialog",Font.BOLD,11));
        Label1 = new Label("Chromosome Length",Label.LEFT);
        Label1.setFont(new Font("Dialog",Font.BOLD,11));
        Label2 = new Label("Mutation probability ",Label.LEFT);
        Label2.setFont(new Font("Dialog",Font.BOLD,11));
        Label3 = new Label("Crossover probability",Label.LEFT);
        Label3.setFont(new Font("Dialog",Font.BOLD,11));
        Proceed = new Button("Proceed");
        Proceed.setFont(new Font("Dialog",Font.BOLD,12));
        Proceed.enable(false);
        // End of Component Initialization

        // Add()s
        add(Proceed);
        add(Label3);
        add(Label2);
        add(Label1);
        add(Population);
        add(TextPop);
        add(TextPc);
        add(TextPm);
        add(text);
        add(TextChrom);
        // End of Add()s

        InitialPositionSet();
    }

    public void InitialPositionSet()
    {
        // InitialPositionSet()
        reshape(52,120,759,548);
        TextChrom.reshape(206,99+MenuBarHeight,93,23);
        text.reshape(23,132+MenuBarHeight,721,392);
        TextPm.reshape(523,67+MenuBarHeight,92,23);
        TextPc.reshape(524,101+MenuBarHeight,93,22);
        TextPop.reshape(206,65+MenuBarHeight,93,23);
        Population.reshape(33,69+MenuBarHeight,94,19);
        Label1.reshape(30,103+MenuBarHeight,181,19);
        Label2.reshape(345,74+MenuBarHeight,171,19);
        Label3.reshape(345,104+MenuBarHeight,172,19);
        Proceed.reshape(322,30+MenuBarHeight,106,25);
        // End of InitialPositionSet()
    }

    public boolean handleEvent(Event evt)
    {
        // handleEvent()
        if (evt.id == Event.WINDOW_DESTROY && evt.target == this) Boo_WindowDestroy(evt.target);
               StartButton_Action(evt.target);
        // End of handleEvent()

        return super.handleEvent(evt);
    }

    public void paint(Graphics g)
    {
        // paint()
        // End of paint()
    }

   // main()
   public static void main(String args[])
   {
       Boo Boo = new Boo();
       Boo.show();
   } // End of main()

    // Event Handling Routines
    public void Boo_WindowDestroy(Object target)
    {
        System.exit(0);
    }

    // End of Event Handling Routines

} // End o