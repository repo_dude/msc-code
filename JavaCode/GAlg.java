// {$R GAlg.JFM}

import java.awt.*;

// Class GAlg
public class GAlg extends Frame
{
    final int MenuBarHeight = 0;

    // Component Declaration
    public Button Button1;
    public TextField TextField1;
    // End of Component Declaration

    // Constructor
    public GAlg()
    {
        // Frame Initialization
        setForeground(Color.black);
        setBackground(Color.lightGray);
        setFont(new Font("Dialog",Font.BOLD,12));
        setTitle("noname01");
        setLayout(null);
        // End of Frame Initialization

        // Component Initialization
        Button1 = new Button("Button1");
        Button1.setFont(new Font("Dialog",Font.BOLD,12));
        TextField1 = new TextField("TextField1");
        TextField1.setForeground(Color.black);
        TextField1.setBackground(Color.white);
        TextField1.setFont(new Font("Dialog",Font.BOLD,12));
        // End of Component Initialization

        // Add()s
        add(TextField1);
        add(Button1);
        // End of Add()s

        InitialPositionSet();
    }

    public void InitialPositionSet()
    {
        // InitialPositionSet()
        reshape(144,126,400,300);
        Button1.reshape(53,61+MenuBarHeight,75,25);
        TextField1.reshape(149,61+MenuBarHeight,121,27);
        // End of InitialPositionSet()
    }

    public boolean handleEvent(Event evt)
    {
        // handleEvent()
        if (evt.id == Event.WINDOW_DESTROY && evt.target == this) GAlg_WindowDestroy(evt.target);
        else if (evt.id == Event.ACTION_EVENT && evt.target == Button1) Button1_Action(evt.target);
        // End of handleEvent()

        return super.handleEvent(evt);
    }

    public void paint(Graphics g)
    {
        // paint()
        // End of paint()
    }

   // main()
   public static void main(String args[])
   {
       GAlg GAlg = new GAlg();
       GAlg.show();
   } // End of main()

    // Event Handling Routines
    public void GAlg_WindowDestroy(Object target)
    {
        System.exit(0);
    }

    public void Button1_Action(Object target)
    {
     short a=Short.parseShort("10001",2);
     TextField1.setText(Short.toString(a));       
    }

    // End of Event Handling Routines

} // End of Class GAlg
