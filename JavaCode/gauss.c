#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <process.h>
#define N 4

/* Solving Ax=B using Gauss eliminations method */
/* the matrix A augmnted with b will be stored in one vector  A  */

//double A[]={2,1,1,1,30,1,2,1,1,40,1,1,2,1,50,1,1,1,2,60};
double *A ;
/*====================== fuction prototypes ==================*/
double* allocateMem(int);
void setA(int ,int ,double);
double valA(int,int);
void setB(int , double);
double valB(int );
void Gauss();
/*============================================================*/

int main(){

	int  i,j;
	A = allocateMem(N);

	for(i=0;i<N;i++)
		for(j=0;j<N;j++) setA(i,j,(double)rand());
	for(i=0;i<N;i++) setB(i,(double)rand());
	printf("\n*------------------------*\n");
	for(i=0;i<N;i++){
	 for(j=0;j<N;j++) printf(" %f",valA(i,j));
	 printf("\n");
	}
	printf("\n");
	for(i=0;i<N;i++) printf(" %f",valB(i));

    printf("\n");
    Gauss();
    free(A);
	return 0;
}
/*================= finding memory for a(i,j) & b(i) ================*/
double* allocateMem(int dim){
	double* ptr;
	ptr = malloc(dim*(dim+1)*sizeof(double));
	return ptr;
}
/*======================= accessing a(i,j) & b(i) ====================*/
void setA(int i, int j ,double val){
	A[(N+1)*i+j]=val;
}
double valA(int i, int j){
	return A[(N+1)*i+j];
}
void setB(int i, double val){
	A[i*(N+1)+N]=val;
}
double valB(int i){
	return A[i*(N+1)+N];
}
/*========================= solving a.x = b ===========================*/
void Gauss(){

  double coeff;
  const int n=4;
  double x[4];
  double d,s;
  int i,j,k;
		 /* triangularization */
  for(k=0;k<n-1;k++)
   for(i=k+1;i<n;i++){
   		coeff =valA(i,k)/valA(k,k);
		setB(i,valB(i)-coeff*valB(k));
	    for(j=k+1;j<n;j++) setA(i,j, valA(i,j)-coeff*valA(k,j) );
   }
         /*******************/
  for(i=n-1;i>=0;i--){
  	  for (s=0.0,j=i+1;j< n;j++) s+=valA(i,j)*x[j];
	  x[i]=(valB(i)-s)/valA(i,i);
  }
  printf(" Solution: \n");
  for(i=0;i<n;i++) printf("  %10.6f",x[i]);
  printf("\n\n");
}
/*============================= end ====================================*/
