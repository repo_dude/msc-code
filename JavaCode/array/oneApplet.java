/* this is an applet example */

import java.awt.*;
import java.applet.Applet;

public class oneApplet extends Applet {
 
  public void init() {
      repaint();
  } 
  
  public void paint(Graphics g) {
      table(g,4,7);
  }
  private void table(Graphics g,int x,int y) {
      private int pace=30;
      private int i,j;
      //g.setColor(Color.red);
      g.drawRect(50,50,50+pace*x,50+pace*y);
      for(i=1;i<=6;i++) 
        g.drawRect(50+i*pace,50,50+i*pace,50+y*pace);

  }

}