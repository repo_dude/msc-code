/* 
  Tutorial 3
  Author Kamel saadi
*/  

import java. awt .* ; 
import java.applet.Applet; 
import java.awt.event.*; 
import javax.swing.Timer; 

public class TrivialApplet extends Applet implements MouseListener,
  ActionListener{ 
     private int      x = 50; 
     private int      y = 80; 
     private int      count=0;
     private int      side=20; 
     private Graphics g; 
     private Timer    timer; 
     
     public void init() { 
         addMouseListener(this);
         /* random elapsing time for the 'Creature' appearence */
         int lap=(int)(Math.random()*1000)+800;
         timer=new Timer(lap,this) ; 
         timer.start() ; 
     } 
     public void paint (Graphics g) { 
         x=(int)(Math.random()*200)+1; 
         y=(int)(Math.random()*200)+1; 
         g.drawString("Your score is ;"+count, 50,250) ; 
         g.setColor (Color.red); 
         g.fillRect(x, y, side, side);  
     } 
     public void actionPerformed(ActionEvent event){ 
         repaint(); 
     } 
 
     public void mouseClicked(MouseEvent e){ 
         int whereX = e.getX(); 
         int whereY = e.getY();
         /* checking if the mouse pointer hits the ball */ 
         if (whereX>=x && whereX<=x+side && 
             whereY>=y && whereY<= y+side){
              play(getCodeBase(), "tiptoe.thru.the.tulips.au");
              count++;
         }
                
     } 
     public void mousePressed(MouseEvent e){
     } 
     public void mouseReleased(MouseEvent e){
     } 
     public void mouseEntered (MouseEvent e) {
     } 
     public void mouseExited(MouseEvent e){
     } 
} 
