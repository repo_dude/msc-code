import java.awt.*;
import java.awt.event.*;

public class prog extends Frame implements ActionListener,
              WindowListener {

        private Button right;
        private Button left;
        private Balloon Ball;

        public static void main(String[] args) {
                prog f = new prog();
                f.setSize(300,300);
                f.setVisible(true);
        }            
                
     
        public prog() {
                setTitle("Balloon");
                setLayout( new FlowLayout());
                Ball = new Balloon(50, 100, 100, Color.red, "Dude");

                left = new Button("<--Left");
                add(left);
                left.addActionListener(this);

                right = new Button("Right-->");
                add(right);
                right.addActionListener(this);

                this.addWindowListener(this);
        }
        public void actionPerformed(ActionEvent evt)
        {

                if (evt.getSource() == left)
                        Ball.moveLeft();
                if (evt.getSource() == right)
                        Ball.moveRight();
                repaint();
        }
        public void windowClosing(WindowEvent event){
                System.exit(0);
        }        
        public void windowOpened(WindowEvent event) {
        }
        public void windowClosed(WindowEvent event) {
        } 
        public void windowIconified(WindowEvent event) {
        }
        public void windowDeiconified(WindowEvent event) {
        }
        public void windowActivated(WindowEvent event) {
        }
        public void windowDeactivated(WindowEvent event) {
        }
        public void paint(Graphics g)
        {
                Ball.display(g);
        }

}          

class Balloon
{

        private int diameter;
        private int xCoord;
        private int yCoord;
        private Color Col;
        private String name;

        public Balloon(int initialDiameter, int initialX, int initialY,
                Color theColor, String theName)
        {

                diameter = initialDiameter;
                xCoord = initialX;
                yCoord = initialY;
                Col = theColor;
                name = theName;
        }
        public void display(Graphics g)
        {
                g.setColor(Col);
                g.fillOval(xCoord, yCoord, diameter, diameter);
                g.setColor(Color.black);
                g.drawString(name, xCoord, yCoord);
        }
        public void moveRight()
        {
                xCoord += 20;
        }
        public void moveLeft()
        {
                xCoord -= 20;
        }
}







