import java.awt.*;
import java.applet.Applet;

public class Balloon {

          private int diameter;
          private int xCoord;
          private int yCoord;
          private Color Col;
          private String name;
    
          public Balloon(int initialDiameter, int initialX, int initialY, Color theColor,  String theName) {
                    
                    diameter = initialDiameter; 
                    xCoord  = initialX;
                    yCoord  = initialY;
                    Col = theColor;
                    name = theName;
           }
           public void display(Graphics g) {
                   g.setColor(Col);
                   g.fillOvall(xCoord,yCoord,diameter,diameter);
                   g.setColor(Color.black);
                   g.drawString(name,xCoord,yCoord);
           }
           public void moveRight() {

                    xCoord+=20;
           }
           public void moveLeft() {
                    
                    xCoord-=20;
           }
}
   
