import java.awt.*;
import java.applet.Applet;
import java.awt.event.*;

public class oneApplet extends Applet implements ActionListener
{

        private Button right;
        private Button left;
        private Balloon Ball;

        public void init()
        {

                Ball = new Balloon(50, 100, 100, Color.red, "Dude");

                left = new Button("<--Left");
                add(left);
                left.addActionListener(this);

                right = new Button("Right-->");
                add(right);
                right.addActionListener(this);
        }
        public void actionPerformed(ActionEvent evt)
        {

                if (evt.getSource() == left)
                        Ball.moveLeft();
                if (evt.getSource() == right)
                        Ball.moveRight();
                repaint();
        }
        public void paint(Graphics g)
        {

                Ball.display(g);
        }


public class Balloon
{

        private int diameter;
        private int xCoord;
        private int yCoord;
        private Color Col;
        private String name;

        public Balloon(int initialDiameter, int initialX, int initialY,
                Color theColor, String theName)
        {

                diameter = initialDiameter;
                xCoord = initialX;
                yCoord = initialY;
                Col = theColor;
                name = theName;
        }
        public void display(Graphics g)
        {
                g.setColor(Col);
                g.fillOval(xCoord, yCoord, diameter, diameter);
                g.setColor(Color.black);
                g.drawString(name, xCoord, yCoord);
        }
        public void moveRight()
        {

                xCoord += 20;
        }
        public void moveLeft()
        {

                xCoord -= 20;
        }
}
}






