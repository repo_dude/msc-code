/*
	Trivial applet that displays a string - 4/96 PNL
*/

import java.awt.*;
import java.applet.Applet;

public class TrivialApplet  extends Applet {
    private Image    image;
    private String   location="C:\\desktop\\sample\\";
    
      public void init(){
         image = getImage(getDocumentBase(),location+"1002.gif");
      }
      public void paint(Graphics g) {
          boolean b = g.drawImage(image,20,20,100,100,this);
      }
}               