@echo off
rem *--------------------------------------------------------------------*
rem handling java applications with arguments using Turbo C++ editor.
rem                                                              K. saadi
rem *--------------------------------------------------------------------*
rem Normal usage:
rem     compiling : javac javafile.java
rem     running   : java  javafile arg1 arg2 arg3.....
rem Usage with do.bat : do javafile(.cpp not included) arg1 arg2 ....
cls
if exist %1.java  del %1.java
if exist %1.class del %1.class
ren %1.cpp %1.java
@echo compiling...
javac %1.java
if not exist %1.class goto failed
cls
@echo running...
@echo.
java %1 %2 %3 %4 %5 %6 %7 %8 %9 %10
@echo.
pause
goto done
:failed
@echo compile failed.
:done
ren %1.java %1.cpp
exit
