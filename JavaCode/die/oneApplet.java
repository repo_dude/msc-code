/*
	Trivial applet that displays a string - 4/96 PNL
*/

import java.awt.*;
import java.applet.Applet;
import java.awt.event.*;

public class oneApplet extends Applet
      implements ActionListener {
      private Button throwDice; 
      private Dice dice1,dice2;
      private boolean click=false;

      public void init() {
             
             dice1=new Dice();
             dice2=new Dice();
             
             throwDice = new Button(" throw ");
             add(throwDice);
             throwDice.addActionListener(this);
                          
      } 
      public void actionPerformed(ActionEvent evt) {
             
             click=true;
             repaint();
      }
      public void paint(Graphics g) {
             if (click) g.drawString("Dice1 = "+dice1.getResult()+"   Dice2 = "+dice2.getResult(),40,50);
             else  g.drawString("Dice1 =  ?   dice2 = ?",40,50);
             
      }
      
      
} 

class Dice {
         
         private int result;
            
         public int getResult() {
         
               result=(int)(Math.random()*6)+1; 
               return result;
         }
}                        












   
      