// java application template.                           

import java.awt.*;
import java.awt.event.*;

public class prog extends Frame implements ActionListener,
              WindowListener {

        // declareations      
        private Button throwDice; 
        private Dice dice1,dice2;
        private boolean click=false;
        
        public static void main(String[] args) {
                prog f = new prog();
                f.setSize(300,300);
                f.setVisible(true);
        }            
                
     
        public prog() {             

             // add here all the widgets 
             setTitle("Casino");
             setLayout(new FlowLayout());

             dice1=new Dice();
             dice2=new Dice();
             
             throwDice = new Button(" throw ");
             add(throwDice);
             throwDice.addActionListener(this);

             this.addWindowListener(this);

        }
        public void actionPerformed(ActionEvent evt)
        {
         click=false;
         if (evt.getSource()==throwDice) click=true;
         repaint();   
        }
        public void windowClosing(WindowEvent event){
                System.exit(0);
        }        
        public void windowOpened(WindowEvent event) {
        }
        public void windowClosed(WindowEvent event) {
        } 
        public void windowIconified(WindowEvent event) {
        }
        public void windowDeiconified(WindowEvent event) {
        }
        public void windowActivated(WindowEvent event) {
        }
        public void windowDeactivated(WindowEvent event) {
        }
        public void paint(Graphics g)
        {
             if (click) g.drawString("Dice1 = "+dice1.getResult()+"   Dice2 = "+dice2.getResult(),50,100);
             else  g.drawString("Dice1 =  ?   dice2 = ?",50,100);

        }

}          

class Dice {
         
         private int result;
            
         public int getResult() {
         
               result=(int)(Math.random()*6)+1; 
               return result;
         }
}                        








