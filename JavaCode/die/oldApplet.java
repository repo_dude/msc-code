import java.awt.*;
import java.applet.Applet;
import java.awt.event.*;

public class oneApplet extends Applet
      implements ActionListener {
      private Button throwing; 
      private Die myDie;
      private Boolean press=false; 

      public void init() {
             myDie=new Die(0);
             throwing = new Button("throw");
             add(throwing);
             throwing.addActionListener(this);
      } 
      public void actionPerformed(ActionEvent evt) {
             press=false;
             if (evt.getSourse()==throwing) press=true
             repaint();
      }
      public void paint(Graphics g) {
             g.drawString("Result = ? ",100,50);
             if (press) g.drawString("Result = "+myDie.getResult(),100,50);
              
      }
      
      class Die {
            private int result;
            public Die(int x) {
                 result=6;
            }
            public int getResult() {
               return result;
            }
      }
}                         

       
      