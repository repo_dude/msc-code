// this application shows the use of the output stream.                           

import java.io.*;
import java.awt.*;
import java.awt.event.*;

public class prog extends Frame implements ActionListener,
              WindowListener {

        // data declareations
        private TextArea    inputText;
        private Label       file;  
        private TextField    fileName;
        private Button      save;
        private PrintWriter outFile; // outFile is an output stream object
        
              
        public static void main(String[] args) {
                prog f = new prog(); // instanciation of the application class prog
                f.setSize(300,300);
                f.setVisible(true);
        }            
                
     
        public prog() {
               setLayout( new FlowLayout());
               
               save = new Button("save");
               add(save);
               save.addActionListener(this);
               
               file= new Label(" File :");
               add(file);
               
               fileName = new TextField(20);
               add(fileName);
               
               inputText = new TextArea(40,50);
               add(inputText);
               
               this.addWindowListener(this);
        }
        public void actionPerformed(ActionEvent evt)
        {
           if(evt.getSource()==save) {
             try {
                 outFile = new PrintWriter(
                                new FileWriter(fileName.getText()),true);
                 outFile.print(inputText.getText() );
                 outFile.close();
            }
            catch (IOException e) {
                 System.err.println("File Error: "+ e.toString());
                 System.exit(1);
            }
           }            
        }
        public void windowClosing(WindowEvent event){
                System.exit(0);
        }        
        public void windowOpened(WindowEvent event) {
        }
        public void windowClosed(WindowEvent event) {
        } 
        public void windowIconified(WindowEvent event) {
        }
        public void windowDeiconified(WindowEvent event) {
        }
        public void windowActivated(WindowEvent event) {
        }
        public void windowDeactivated(WindowEvent event) {
        }

}          







