/*
 * Multiple Knapsack problem
 * 
 *    MAX (SUM{pi*xi})             i=1,..,n
 *
 *    subject to SUM{wij*xj}<= bi  j=1,..,n; i=1,..,m
 *
 *    wij,bi,pi >=0
 * 
 * K. Saadi 
 *
 */
 
import java.awt.*;
import java.awt.event.*;

class MKP extends Frame
{   

    public int       maxIters= 20;    
    public TextArea  text; 
    public KnapSacks knps;              
 
 	public MKP()
	{
		text = new TextArea(40,300);
		text.setBackground(Color.black);
        text.setForeground(Color.green);
        text.setFont(new Font("Dialog",Font.BOLD,14));
 		add(text);
		
		knps = new KnapSacks(10,2); 
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				dispose();
				System.exit(0);
			}
		});
	}
	
	public static void main(String args[])
	{   
		System.out.println("Starting MKP...");
		MKP mkp = new MKP();
		mkp.setSize(650, 450);
		mkp.setTitle("MKP");
		mkp.setVisible(true);
  		double max=0;
		//int[]  xOpt=new int[mkp.knps.n];
        mkp.text.appendText(mkp.knps.toString());
	    //max = mkp.knps.value();
        for(int i=0;i<40;i++){
            mkp.knps.localSearch();
            //if(mkp.knps.newIsBetter())
            mkp.text.appendText(mkp.knps.printSol());
	    }
	 }  
}


class KnapSacks{
	public int         n; // the number of objects xi
	public int         m; // the number of knapsacks
	public double[]    p; // profits pi
	public double[][]  w; // weights wij; j=1,...,n ; i=1,...,m
	public double[]    c; // knapsack capacities cj
	public double      f; // objective function f= SUM(wij*xj)
	public int[]       x;         
	public int[]       newX;
	
	public KnapSacks( int n0, int m0 ){
		n = n0;
		m = m0;
		p = new double[n]; 
	    x    = new int[n]; 
		newX = new int[n];
		c = new double[m];
		w = new double[m][n];
		f=0;
		setProblem();
		generateInitialSol();
	}
	
	// if stat=0 this method generates an initial solution
	// otherwise it generates a new solution by flipping a random selected 
	// bit, of in both cases the solutions have to be feasible
	public void generateInitialSol(){ 
		 do {
		     for (int i=0;i<n;i++){
		       if(Math.random()<0.5) x[i]=1; // generating a random
		                      else   x[i]=0; // initial feasible solution
             }
         }while (!feasible(x));
         f=value(x);
    }
    	
	public void affect(int[] x1,int[] x2){
		for(int i=0;i<n;i++) x1[i]=x2[i];
	}	
			
	public void generateNeighbourSol(){ 
           int pos;
           do{
           	  pos = (int)(Math.random()*n);
           	  affect(newX,x);
           	  if (newX[pos]==0) newX[pos]=1;
           	  else newX[pos]=0;
           }while (!feasible(newX));
    }           	   
		
	public void setProblem(){
		for(int i=0;i<n;i++) p[i]=random(10); //  generating profits pi 
		
		for(int i=0;i<m;i++){	
	   		c[i] = 10+random(20);// generating capacities ci
			for(int j=0;j<n;j++)
				w[i][j]=random(10); // generating weights wij
		}
	}			
	
	private double random(int N){
		double rand = (double)((int)(Math.random()*N))+1;
		return rand;
	}
	
	public boolean feasible(int[] x){
		boolean can = true;
		double  sum;
		int     i=0; 
		while (can && i<m){	
	   		 sum =0;
	   		 for(int j=0;j<n;j++)
		        sum+=w[i][j]*x[j];
		     can = (sum<=c[i]);
		     if (can==false) break;
		     i++;    
		}
		return can;
	}	     
		
	public double value(int[] x){
		if (feasible(x)){
			double sum=0;
		    for(int i=0;i<n;i++) sum+=p[i]*x[i];
	        return sum;
	       }
	    else return -1;    
	}
		
	//public boolean newIsBetter(){
	//	boolean result=false;
	//	result=(value(newX)>f);
	//	return result;
	
	
	public void localSearch(){
		generateNeighbourSol();
		if (value(newX)>value(x)) {
			affect(x,newX);
			//f=value(newX);
		}	
		//printSol()
	}		
	
	public String toString(){
		String report="\n\nPROBLEM DETAILS:\n\n";
		report+="PROFITS:";
		 for(int i=0;i<n;i++) report+="\t"+Double.toString(p[i]);
		report+="\n\t\t\tWEIGHTS\t\t\t                    CAPACITIES:";
		int i=0;
		while (i<m){
		  report+="\n";
		  for(int j=0;j<n;j++) report+="\t"+Double.toString(w[i][j]); 
		  report+="\t"+c[i];
		  i++;
		} 
		report+="\nSOLUTION\n";
		for (i=0;i<n;i++)
			report+="\t"+x[i]; 
			return report;
	}
	public String printSol(){
		String	report="\n";
		for (int i=0;i<n;i++)
			report+="\t"+x[i];
		report += "\t"+value(x);	
		return report;	
	}		 	 
}		