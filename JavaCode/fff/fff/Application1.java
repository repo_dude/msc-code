/* 
	Application1.java

	Author:			kamel
	Description:	
*/

package fff;

public class Application1 
{
	public Application1() 
	{
		try {
			Frame1 frame = new Frame1();
			frame.initComponents();
			frame.setVisible(true);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Main entry point
	static public void main(String[] args) 
	{
		new Application1();
	}
	
}
