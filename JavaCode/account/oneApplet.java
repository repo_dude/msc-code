import java.awt.*;
import java.applet.Applet;
import java.awt.event.*;

public class oneApplet extends Applet implements ActionListener 
{
	private TextField  nameField;
      private TextField  accountNumberField;
      private TextField  balanceField;
      private TextField  depositField;
      private TextField  withdrawField;   
	
      private Label      accountNumberLabel;
      private Label      nameLabel;
      private Label      balanceLabel;
      private Label      depositLabel;
      private Label      withdrawLabel; 
	
      private Account    customer;
      private int        numberIn; 
	private int        amountIn=0,amountOut;
	
	public void init() {
	    
	    customer = new Account(03336013,"James Frasier");
	    
          nameLabel = new Label(" Name          : ",Label.LEFT);
	    nameField = new TextField(20);
          
          accountNumberLabel = new Label(" Account Number : ",Label.LEFT);
	    accountNumberField = new TextField(8);
	    
          balanceLabel = new Label(" Balance        : ",Label.LEFT);
	    balanceField = new TextField(12);
	    	    
          depositLabel = new Label(" Deposit        : ",Label.LEFT);
          depositField = new TextField(12);
          
          withdrawLabel = new Label(" Withdraw        : ",Label.LEFT);
          withdrawField = new TextField(12);

	    
          add(nameLabel);
          add(nameField); 
          add(accountNumberLabel);
	    add(accountNumberField);
          add(balanceLabel);
          add(balanceField);
          add(depositLabel);
          add(depositField);
          add(withdrawLabel);
          add(withdrawField);

          nameField.addActionListener(this);
          accountNumberField.addActionListener(this);
          balanceField.addActionListener(this);
          depositField.addActionListener(this);
          withdrawField.addActionListener(this);
	     
          //repaint();
	}
	
	public void actionPerformed(ActionEvent event) {
           if(event.getSource() == depositField)	
	       amountIn  = Integer.parseInt(depositField.getText());
	     if(event.getSource() == withdrawField)  
             amountOut = Integer.parseInt(withdrawField.getText());
	       repaint();
	}       
	    
	public void paint( Graphics g ) {
		
	     nameField.setText(customer.getName());
           accountNumberField.setText( Integer.toString(customer.getNumber()));
           customer.deposit(amountIn);
           amountIn=0;
           customer.withdraw(amountOut);
           amountOut=0;
           depositField.setText("");
           withdrawField.setText("");
           balanceField.setText(Integer.toString(customer.getBalance()));
           
		
	}



public class  Account {
       
       protected String  name;
       protected int     accNumber;  
       protected int     balance=0;
       
       public Account(int number,String theName) {
             accNumber=number;
             name=theName;
           
       }
       public String getName() {
            return name;
       }
       public int getNumber()  {
            return accNumber;
       }
       public int getBalance() {
            return balance;
       }
       
       public void deposit(int newAmount) {
            balance = balance + newAmount;
       }     
       
       public void withdraw(int moneyOut) {
            balance = balance - moneyOut;
       } 
       
} 
             
public class checkAccount extends Account {

       private final int limit=100; // overdraft limit 100
       
       public checkAccount(int number,String theName) {
           
            accNumber=number;
            name=theName;
       }
              
       public void withdraw (int cash)  {

          if ((balance-cash)>=-limit) 
             balance = balance - cash;
       }
}

public class saveAccount extends Account {
       
       private final double rate= 0.03f;

       public saveAccount(int number,String theName) {
            
             accNumber = number;
             name = theName;
 
       }             
       public void withdraw (int cash)  {
          if ((balance-cash)>=0)
             balance = balance - cash;
       }
       public void addInterest() {
          balance = balance + 1 ;//(int)((float)balance*rate);
       }
}
    
       


}

       

