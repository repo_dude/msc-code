

import java.text.*;

public class Tunes{

    public static void main (String[] args){
       ListD music = new ListD();
       music.addCD("Jill Scot","Who is Jill Scott",12.99,20);
       music.addCD("Goldfrapp","Felt Mountain",12.99,12);
       music.addCD("Sigur Ros","Agaetis Byrjun",14.99,10);
       music.addCD("Nitin Sawhney","Broken Skin",10.99,14);
       System.out.print(music);
    }
}





class ListD{

      private double totalValue;
      private CDNode head;
      private CDNode browse;
      private int    length=0;

      public ListD() {
      }
      // in order to maintain a list we always need to know it's head
      // head is a reference to the first item in the list
      //**** Creating the list ****//
      public void addCD(String artist,String title,double cost, int tracks){

	 CDNode pos;
	 CD aCD = new CD(artist,title, cost, tracks);
	 CDNode temp= new CDNode(aCD);

	 if (length==0) {
	     head  = temp;
	 }
	 else {
	    pos=head;
	    int l;
	    while(pos!=null){
	       int n = pos.theCD.artist.compareTo(temp.theCD.artist);
	       if (pos.next==null) l=1;
		 else l = pos.next.theCD.artist.compareTo(temp.theCD.artist);
	       if (n<=0 && l>=0) {
		 insert(pos,temp);
	       }
	       if (n>0) insert(head,temp);
	       pos=pos.next;
	    }
	 }
	 length++;
	 totalValue+=cost;
      }

      public void insert(CDNode nodeA,CDNode nodeB) {
	   if (nodeA==head) {
		 nodeB.next=head;
		 head=nodeB;
	   }
	   else if(nodeA.next==null) nodeA.next=nodeB;
		else {
		  nodeB.next=nodeA.next;
		  nodeA.next =nodeB;
		}
      }

      /*---------------------- reading the list -------------------------*/
      public String toString(){
	 String report="";
	 browse=head;
	 while(browse!=null){
	     report += browse.theCD.toString()+"\n";
	     browse=browse.next;
	 }
	 return report;
      }

      /*------------------------------------------------------------------*/
      /*--------------------- inner classes ------------------------------*/
      /*------------------------------------------------------------------*/

      public class CDNode {
	    public CD     theCD;
	    public CDNode next;

	    public CDNode(CD newCD){
		  theCD = newCD;
		  next  = null;
	    }
      }

      public class CD {

	    private String  artist;
	    private String  title;
	    private double  cost;
	    private int     tracks;

	    public CD(String xArtist,String xTitle,double xCost, int xTracks)
	    {
		artist = xArtist;
		title  = xTitle;
		cost   = xCost;
		tracks = xTracks;
	    }

	    public String toString() {
		String out;
		out= artist+"\t"+title+"\t�"+cost+"\t"+tracks;
		return out;
	    }

      }
}



