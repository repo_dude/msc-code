/* this is an applet example */

import java.awt.*;
import java.applet.Applet;

public class oneApplet extends Applet {
 
  public void init() {
      repaint();
  } 
  
  public void paint(Graphics g) {
      g.drawString("This is great "+dull(5),50,50);
      square(g,45,45,200);
  }
  private void square(Graphics g,int x,int y,int side) {
      g.setColor(Color.red);
      g.fillRect(x,y,x+side,y-side);
  }
  private int dull(int k){
   return k*k;
  } 

}