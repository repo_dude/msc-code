/* this is an applet example */

import java.awt.*;
import java.applet.Applet;

public class Square extends Applet {
 
  public void init() {
      repaint();
  } 
  
  public void paint(Graphics g) {
     
      g.drawString("This is great",50,50);
      g.drawLine(10,10,100,100);
      square(g,45,45,200);
  }
  private void square(Graphics g,int x,int y,int side) {
      g.setColor(Color.red);
      g.fillRect(x,y,x+side,y-side);
  }
}