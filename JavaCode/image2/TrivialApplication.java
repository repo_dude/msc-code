import gui.*;
import java.io.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;

public class Browser extends 
    ImageFrame implements ActionListener
    {
  
	private  String    path;
	//private  Image     image; image is inherited from ImageFrame 
	private  Button    back; 
	private  Button    next;
	private  String[]  images;
	
	private  File      myDirectory; 
	private  int       index=0;
	private  String    imageFile;
	private  Label     location;
	private  Button    set;
	private  boolean   loaded;
	private  Vector    imVector;
	private  Button    rand;
	private  Image[]   imgs;
	private  Image     xImage;  
	
	public Browser()
	{
	
	      super("Tiny Image Browser");
	      setBackground(Color.gray);
	      setLayout(new FlowLayout() );
	      	      
	      back = new Button("<- back ");
	      next = new Button(" next ->");
	      add(back);
	      add(next);
          location = new Label("   Location: ");
          add(location);
          
          set = new Button("Set Image Database");
          add(set);
          
          rand = new Button(" Random Display ");
          add(rand);
          
          loaded=false;
          
          next.addActionListener(this);
          back.addActionListener(this);
          set.addActionListener(this);
          rand.addActionListener(this);
                
    }      	
	
	
	
	
	
	public static void main(String args[]) {
	     Browser bs = new Browser();
	     bs.setSize(600,470);
	     bs.setVisible(true);
	     //bs.openGif(bs.imageFile);
	     
	
	}

    public void paint(Graphics screen) {
        if(loaded){ 
         imageFile=(String)imVector.elementAt(index);
         //openGif(path+imageFile);
         //Toolkit toolkit = Toolkit.getDefaultToolkit();
         //image = toolkit.getImage(path+imageFile);
         boolean b= screen.drawImage(image,10,80,580,400,this);
        } 
    }       
    
    private Frame f = new Frame();	
	
	public void setDirectory() {
	     FileDialog fd = new FileDialog(f,"Images");
	     fd.show();
	     path=fd.getDirectory();
	     fd.dispose();
	     if (path!=null) load();
	}       
	      
	
	public void load() {
	     
	     imVector= new Vector();
	     myDirectory = new File(path);
         boolean checkDirectory = myDirectory.isDirectory();
         
         if(checkDirectory){
             
             loaded=true;  
             String[] temp=myDirectory.list();
             // filtering files: loading only the image file names.        
             int k=0;
             for(int i=0; i<=temp.length-1;i++) 
              if ((temp[i].toUpperCase().indexOf(".GIF",0)>0) || 
               (temp[i].toUpperCase().indexOf(".JPEG",0)>0) || 
               (temp[i].toUpperCase().indexOf(".JPG",0)>0)  ) {
                    imVector.addElement(temp[i]);    
                    
              }    
               
          } 
                 //images=temp;
            
    }
     
    public void next() {
         
         String fn;
         if (index==imVector.size()-1) index=0;
         fn=(String)imVector.elementAt(index++);
         openGif(path+fn);     
    }     
             
    public void back() {
         
         String fn;
         if (index==0) index=imVector.size()-1;
         fn= (String)imVector.elementAt(index--); 
         openGif(path+fn);
    }     
    
	
	public void actionPerformed (ActionEvent evt ) {
	
	     if (evt.getSource()==next) next();
	     if (evt.getSource()==back) back();
	     if (evt.getSource()==set) {
	       setDirectory();
	       location.setText("   Location: "+path);
         }
         if (evt.getSource()==rand) {
            //Graphics g=getGraphics();
            RandomSet rs = new RandomSet(imVector,path);
            rs.setSize(600,600);
            //= getGraphics();
            //rs.display();
            rs.show();
                    
         }   
            
         super.actionPerformed(evt);        
	     repaint();
	}
	

}

///////////////////////////////////////////////////////////////////////////////////////////

class RandomSet extends ClosableFrame {
       
       private Vector    vector;
       private int[]     select;
       private String    path;
       private Graphics  gp;
       private Toolkit   toolkit=Toolkit.getDefaultToolkit();
       private Image[]   images;   
       
       
       public RandomSet(Vector vect,String aPath){
          
          path = aPath;
          vector = new Vector();
          vector = vect;
          //gp=g;
          images = new Image[9];
          randomize();
          loadImages();
       }
       
       //public static void main( String[] args) {
       
        // RandomSet rs = new RandomSet(vect);
        // rs.setVisible(true);
       //} 
       
       public void loadImages() {
         
          String fn;
          for(int i=0;i<9;i++) { 
            fn = (String) vector.elementAt(select[i]);
            images[i] = Toolkit.getDefaultToolkit().getImage(path+fn);
          } 
       }  
       
       public void paint(Graphics g) {
         
	    //for(int i=0; i<9 ;i++){
	     //int x = (i%3);
	     //int y = (i - i%3)/3; 
	     
         g.drawImage(images[0],0,0,200,200,this);
         g.drawImage(images[1],200,0,200,200,this);
         g.drawImage(images[2],400,0,200,200,this);
         g.drawImage(images[3],0,200,200,200,this);
         g.drawImage(images[4],200,200,200,200,this);
         g.drawImage(images[5],400,200,200,200,this);
         g.drawImage(images[6],0,400,200,200,this);
         g.drawImage(images[7],200,400,200,200,this);
         g.drawImage(images[8],400,400,200,200,this);

        
      }
      
      public void randomize(){
      
        select = new int[9];
        for(int i=0; i<9;i++)         
           select[i]=(int)( Math.random()*10000)%vector.size() ;
               
      }
      
    
           
} 