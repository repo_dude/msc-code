// {$R noname01.JFM}

import java.awt.*;

// Class noname01
public class noname01 extends Frame
{
    final int MenuBarHeight = 0;

    // Component Declaration
    // End of Component Declaration

    // Constructor
    public noname01()
    {
        // Frame Initialization
        setForeground(Color.black);
        setBackground(Color.lightGray);
        setFont(new Font("Dialog",Font.BOLD,12));
        setTitle("noname01");
        setLayout(null);
        // End of Frame Initialization

        // Component Initialization
        // End of Component Initialization

        // Add()s
        // End of Add()s

        InitialPositionSet();
    }

    public void InitialPositionSet()
    {
        // InitialPositionSet()
        reshape(316,149,400,300);
        // End of InitialPositionSet()
    }

    public boolean handleEvent(Event evt)
    {
        // handleEvent()
        if (evt.id == Event.WINDOW_DESTROY && evt.target == this) noname01_WindowDestroy(evt.target);
        // End of handleEvent()

        return super.handleEvent(evt);
    }

    public void paint(Graphics g)
    {
        // paint()
        // End of paint()
    }

   // main()
   public static void main(String args[])
   {
       noname01 noname01 = new noname01();
       noname01.show();
   } // End of main()

    // Event Handling Routines
    public void noname01_WindowDestroy(Object target)
    {
        System.exit(0);
    }

    // End of Event Handling Routines

} // End of Class noname01
