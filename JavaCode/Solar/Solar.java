
//////////////////////////////////////////////////////////////////////////////////////////
//  Imaginary Solar system                                                              //
//                                                                             K. Saadi //
////////////////////////////////////////////////////////////////////////////////////////// 
import java.awt.*;
import java.applet.*;
import java.awt.event.*; 

public class Solar extends Applet implements ActionListener
{   
	private Planet        planet;
	private OneSatelite   lunar;
    private TwoSatelites  mars;
	private Jupiter       jup;
    private Clean         clean;
	private Button        start;
	
	public void init()
	{	
		start = new Button("Start");
		add(start);
		start.addActionListener(this);
		setBackground(Color.black);
		
		Graphics g = getGraphics();
		Clean clean = new Clean(g);
		
		lunar = new OneSatelite(g,300,300,170,20,2);
		planet  = new Planet(g,300,300,60,15,6);
		jup   = new Jupiter(g,300,300,110,25,3);
		mars  = new TwoSatelites(g,300,300,210,15,1);
	}
	
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource()==start) {
			Graphics g = getGraphics();
			Clean clean = new Clean(g);
	        planet.start();
	        mars.start();
	        jup.start();
	        lunar.start();
	        clean.start();
		}
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillOval(280,280,40,40);
		int posX,posY,d;
		for(int i=1;i<=800;i++){
			posX=(int)(Math.random()*600)+1;
			posY=(int)(Math.random()*600)+1;
		    d=(int)(Math.random()*2)+1;
		    g.fillOval(posX,posY,d,d);
        }
	
	}	    
	
    class Clean extends Thread {

    	private Graphics g;
    
    	public Clean(Graphics p){
    		g=p;
    	}	
    	public void run()
    	{
	    	repaint();
 			try {
		 	Thread.sleep(60);
			} catch (InterruptedException e) {
				System.err.println("Sleep Exception");
			}	
        }
  	}		

}

class Planet extends Thread{
	
	protected  int       xCentre;
	protected  int       yCentre;
	protected  int       radius;
	protected  int       diameter;
	protected  double    pi=3.14;
	protected  Graphics  g;
	protected  int       c;
	protected  double    t;
	protected  int       dt;
    	
	public Planet(Graphics grph, int x0,int y0,int r, int d0,int cte) {
		g=grph;
		xCentre  = x0;
		yCentre  = y0;
		radius   = r;
		diameter = d0;
		c = cte;
		dt=80;
    }		
	
    public void drawCircle(int x, int y, int r, Color col,boolean shadow) {
       	g.setColor(col);
    	g.fillOval(x-r,y-r,2*r,2*r);
        if (shadow)	makeShadow(x,y,r);
    }
    
    public void makeShadow(int x, int y, int r){
    	int startAngle=90-(int)(180*c*t/pi);	
    	int totalAngle=180;
    	g.setColor(Color.white);
    	g.fillArc(x-r,y-r,2*r,2*r,startAngle,totalAngle);
    } 	
    	
    public void turn(){
			
			int x = xCentre+(int)(radius*Math.cos(c*t));
			int y = yCentre+(int)(radius*Math.sin(c*t));
			drawCircle(x,y,diameter/2,Color.black,false);
			t+=0.02;
		    
		    x = xCentre+(int)(radius*Math.cos(c*t));
			y = yCentre+(int)(radius*Math.sin(c*t));
			drawCircle(x,y,diameter/2,Color.green,true);
			
			if (t==2*pi) t=0;
	}		

	public void run() {
		double t=0; 
		while(t<=2*pi) {	
			turn();
			try {
				Thread.sleep(dt);
			} catch (InterruptedException e) {
				System.err.println("Sleep Exception");
			}	
	    }
	}
}

class Jupiter extends Planet {
	
	private  Graphics g;
	
	public Jupiter(Graphics grph, int x0,int y0,int r, int d0,int cte){ 
		super( grph, x0,y0, r, d0, cte);
		g=grph;
	}	
   
   public void turn(){
		int x = xCentre+(int)(radius*Math.cos(c*t));
		int y = yCentre+(int)(radius*Math.sin(c*t));
		
		drawCircle(x,y,diameter/2,Color.black,false);
		g.setColor(Color.black);
		for(int l=0;l<=5;l++) 
		 g.drawOval(x-diameter/2-10+l,y-diameter/2-10+l,diameter+20-2*l,diameter+20-2*l);
		t+=0.02;
		    
		x = xCentre+(int)(radius*Math.cos(c*t));
		y = yCentre+(int)(radius*Math.sin(c*t));
		drawCircle(x,y,diameter/2,Color.blue,true);
		
		g.setColor(Color.orange);
		for(int l=0;l<=5;l++) g.drawOval(x-diameter/2-10+l,y-diameter/2-10+l,diameter+20-2*l,diameter+20-2*l);
		if (t==2*pi) t=0;
	}		
}	
	
class OneSatelite extends Planet {	
	
	protected   int  relativeRadius=20;
	protected   int  k;
	
	public OneSatelite(Graphics grph, int x0,int y0,int r, int d0,int cte) {
	   super(grph, x0,y0,r, d0,cte);
       k=20;
    }
 
    public void run(){
    	
    	int x,y,x0,y0;
    	for(;;){
    		x0 = xCentre+(int)(radius*Math.cos(c*t));
			y0 = yCentre+(int)(radius*Math.sin(c*t));
			
			drawCircle(x0, y0,diameter/2,Color.magenta,true);
			
			x = x0 + (int)(relativeRadius*Math.cos(k*t));
			y = y0 + (int)(relativeRadius*Math.sin(k*t));
			
			drawCircle(x,y,3,Color.cyan,true); 	
			
			try {
				Thread.sleep(dt);
			} catch (InterruptedException e) {
				System.err.println("Sleep Exception");
			}
				
			x0 = xCentre+(int)(radius*Math.cos(c*t));
			y0 = yCentre+(int)(radius*Math.sin(c*t));
			
			drawCircle(x0, y0,diameter/2,Color.black,false);
			
			x = x0 + (int)(relativeRadius*Math.cos(k*t));
			y = y0 + (int)(relativeRadius*Math.sin(k*t));
			
			drawCircle(x,y,3,Color.black,false);
			
			t= (t==2*pi) ? 0 : t+0.02; 	
           	    
	    }
	}    
}  


class 	TwoSatelites extends OneSatelite{
	
	private int relativeRadius2=30;
	private int speed;
	private Graphics g;
	
	public TwoSatelites(Graphics grph, int x0,int y0,int r, int d0,int cte) 
	{
		super( grph, x0,y0, r, d0, cte);
        speed=k-2;
        g=grph;
        //dt=30; 
    }
    
    public void run(){
    	
    	int x,y,x0,y0;
    	for(;;){
    		x0 = xCentre+(int)(radius*Math.cos(c*t));
			y0 = yCentre+(int)(radius*Math.sin(c*t));
			
			drawCircle(x0, y0,diameter/2,Color.red,true);
			
			x = x0 + (int)(relativeRadius*Math.cos(k*t));
			y = y0 + (int)(relativeRadius*Math.sin(k*t));
			
			drawCircle(x,y,3,Color.blue,true);

			x = x0 + (int)(relativeRadius2*Math.cos(speed*t));
			y = y0 + (int)(relativeRadius2*Math.sin(speed*t));
			
			drawCircle(x,y,3,Color.orange,true);
			
			 	
			
			try {
				Thread.sleep(dt);
			} catch (InterruptedException e) {
				System.err.println("Sleep Exception");
			}
				
			x0 = xCentre+(int)(radius*Math.cos(c*t));
			y0 = yCentre+(int)(radius*Math.sin(c*t));
			
			drawCircle(x0, y0,diameter/2,Color.black,false);
			
			x = x0 + (int)(relativeRadius*Math.cos(k*t));
			y = y0 + (int)(relativeRadius*Math.sin(k*t));
			
			drawCircle(x,y,3,Color.black,false);
			
			x = x0 + (int)(relativeRadius2*Math.cos(speed*t));
			y = y0 + (int)(relativeRadius2*Math.sin(speed*t));
			
			drawCircle(x,y,3,Color.black,false);
			
			
			t= (t==2*pi) ? 0 : t+0.02; 	
           	    
	    }
    }
}	 