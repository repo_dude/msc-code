/*
	Tutorial 1 : transforming the dice applet to an application
	Author     : Kamel Saadi 
*/

// ---------------------------------------------

import java.awt.*;
import java.awt.event.*;

public class TrivialApplication extends Frame implements ActionListener,
	      WindowListener {

	private  Button     throwDice;
	private  Dice       dice;
	private  Dice       dice2;
	private  TextField  winField;
	private  boolean    thrown=false;

	public static void main(String[] args) {
		TrivialApplication f = new TrivialApplication();
		f.setSize(250,180);
		f.setVisible(true);
	}


	public TrivialApplication() {
	      setTitle("Casino");
	      setResizable(false);
	      setBackground(Color.blue);
	      setLayout( new FlowLayout());
	      throwDice = new Button("Throw");
	      add(throwDice);
	      winField= new TextField(10);
	      add(winField);
	      throwDice.addActionListener(this);
	      dice  = new Dice();
	      dice2 = new Dice();
	      this.addWindowListener(this);
	}

	public void paint(Graphics g){
	   if(thrown) {
	    dice.display(g,75,100);
	    dice2.display(g,150,100);
	    if(dice.result==dice2.result)
	       winField.setText(" You win ! ");
	    else
	       winField.setText(" You loose ! ");
	   }
	}

	public void actionPerformed(ActionEvent evt)
	{
	  thrown=false;
	  if(evt.getSource()==throwDice) {
		thrown=true;
		repaint();
	   }

	}
	public void windowClosing(WindowEvent event){
		System.exit(0);
	}
	public void windowOpened(WindowEvent event) {
	}
	public void windowClosed(WindowEvent event) {
	}
	public void windowIconified(WindowEvent event) {
	}
	public void windowDeiconified(WindowEvent event) {
	}
	public void windowActivated(WindowEvent event) {
	}
	public void windowDeactivated(WindowEvent event) {
	}
}

class Dice {

     private int dx=2;
     private int dy=2;
     private int d=10;
     public  int result;

     public Dice() {
     }

     public void display(Graphics g,int coordX,int coordY) {
	 result=(int)(Math.random()*6)+1;
	 g.setColor(Color.red);
	 g.fillRect(coordX,coordY,4*dx+3*d,4*dy+3*d);
	 g.setColor(Color.white);

	 switch(result) {

	   case 1 : g.fillOval(coordX+2*dx+d,coordY+2*dy+d,d,d);
		    break;
	   case 2 : g.fillOval(coordX+dx,coordY+dy,d,d);
		    g.fillOval(coordX+3*dx+2*d,coordY+3*dy+2*d,d,d);
		   break;
	   case 3 : g.fillOval(coordX+2*dx+d,coordY+2*dy+d,d,d);
		    g.fillOval(coordX+dx,coordY+dy,d,d);
		    g.fillOval(coordX+3*dx+2*d,coordY+3*dy+2*d,d,d);
		   break;
	   case 4 : g.fillOval(coordX+dx,coordY+dy,d,d);
		    g.fillOval(coordX+3*dx+2*d,coordY+dy,d,d);
		    g.fillOval(coordX+dx,coordY+3*dy+2*d,d,d);
		    g.fillOval(coordX+3*dx+2*d,coordY+3*dy+2*d,d,d);
		   break;
	   case 5 : g.fillOval(coordX+dx,coordY+dy,d,d);
		    g.fillOval(coordX+3*dx+2*d,coordY+dy,d,d);
		    g.fillOval(coordX+dx,coordY+3*dy+2*d,d,d);
		    g.fillOval(coordX+3*dx+2*d,coordY+3*dy+2*d,d,d);
		    g.fillOval(coordX+2*dx+d,coordY+2*dy+d,d,d);
		   break;
	   case 6 : g.fillOval(coordX+dx,coordY+dy,d,d);
		    g.fillOval(coordX+dx,coordY+2*dy+d,d,d);
		    g.fillOval(coordX+3*dx+2*d,coordY+dy,d,d);
		    g.fillOval(coordX+3*dx+2*d,coordY+2*dy+d,d,d);
		    g.fillOval(coordX+dx,coordY+3*dy+2*d,d,d);
		    g.fillOval(coordX+3*dx+2*d,coordY+3*dy+2*d,d,d);
		    break;
	  }
     }
}