/*
 * GeneticAlgorithm.java 1.0 20/03/2001
 * 
 * Maximasing the function  x*x*x - 60*x*x + 900*x + 100 
 * with x from [0,32]
 *
 * K. Saadi 
 */
 
import java.awt.*;
import java.awt.event.*;

class GeneticAlgorithm extends Frame
{   

    public double   pc               = 0.25; 
    public double   pm               = 0.01; 
    public int      popSize          = 30;
    public int      chromLength      = 22;
    public int      nbrOfGenerations = 20;    
    public TextArea text;   
 
 	public GeneticAlgorithm()
	{
		text = new TextArea(40,300);
		text.setBackground(Color.white);
        text.setForeground(Color.black);
        text.setFont(new Font("Dialog",Font.PLAIN,14));
 		add(text);
		
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				dispose();
				System.exit(0);
			}
		});
	}
	
	public static void main(String args[])
	{   
		System.out.println("Starting GeneticAlgorithm...");
		GeneticAlgorithm ga = new GeneticAlgorithm();
		ga.setSize(650, 450);
		ga.setTitle("GeneticAlgorithm");
		ga.setVisible(true);
	    
	    Population pop    = new Population(ga.popSize,ga.chromLength);
	    Population newPop = new Population(ga.popSize,ga.chromLength);
	  	 
	  	pop.randomSelect();
	  	pop.calculateFitness();
	    pop.probabilities();
        ga.text.appendText("POPULATION SIZE        "+"\t"+pop.pop_size+"\n");
        ga.text.appendText("CHROMOSOME LENGTH      "+"\t"+pop.individual[0].size+"\n");
        ga.text.appendText("MUTATION PROBABILITY   "+"\t"+ga.pm+"\n");
        ga.text.appendText("CROSSOVER PROBABILITY  "+"\t"+ga.pc+"\n");
        ga.text.appendText("\nINITIAL GENERATION  ");
        ga.display(pop);
	    for(int i=1;i<=ga.nbrOfGenerations;i++){
	      ga.text.appendText("GENERATION "+i);
	      newPop = pop.select();
	      newPop.crossover(ga.pc);
	      newPop.mutate(ga.pm); 
	   	  newPop.calculateFitness();
	      newPop.probabilities();
	      ga.display(newPop);
	      pop=newPop;
	    }  
     }
	
	public void display(Population pop){
	    int pos;
	    double optimal;
	    
	    pos     = 0;
	    optimal = pop.individual[pos].fitness;
	    for(int i=1;i<pop.pop_size;i++){
	    	if (optimal<pop.individual[i].fitness){
	    		optimal=pop.individual[i].fitness;
	    		pos = i;
	    	}
	    }		
	    //text.appendText("\n  Chromosome"+"\t\t\t"+"fitness"
	    //	                           +"\t\t\t"+"cummulative probability\n\n");
	    text.appendText("\nOptimal Fitness "+"\t"+optimal);
	    text.appendText("\nAverage Fitness  "+"\t"+pop.globalFitness/pop.pop_size);
	    text.appendText("\nMutations       "+"\t"+pop.mutations);
	    text.appendText("\nCrossovers      "+"\t"+pop.crossovers+"\n\n");
	}
		 
}

class Chromosome{
	
	public int[]   bitString;
	public int     size;
    public double  fitness; // calculated outside the class
	public double  probSelect;
	public double  accumProb ;
	
	public Chromosome( int length){
		bitString   = new int[length];
		size        = length;
		probSelect  = 0.0;
		accumProb   = 0.0;
		fitness     = 0.0;
	}
	
	// decode from binary to decimal
	public int decode(){
	    int powerOf2=1;
	    int s = 0; 
	    for(int i = size-1;i>=0;i--){
	      s = s + bitString[i]*powerOf2;
	      powerOf2 *=2;
	    }
	    return s;
    }
    
    public String toString(){
    	String str="";
    	for(int i=0;i<bitString.length;i++)
    	 str = str + Integer.toString(bitString[i]);
        return str;
    } 
}

class Population{
   
    public   int           pop_size;
    public   Chromosome[]  individual;
    public   Eval          eval;
    public   double        globalFitness;
    public   double        searchSize;
    public   int           crossovers;
    public   int           mutations;       
    
    public Population(int n,int m){
    	
    	pop_size    = n;
    	individual  = new  Chromosome[n];
    	searchSize  = Math.pow(2.0,(double)m);
    	eval        = new Eval(0,32,searchSize);
    	crossovers  = 0;
    	mutations   = 0;
    	
    	for(int i=0;i<n;i++){
    		individual[i]=new Chromosome(m);
    		for(int j=0;j<m;j++)
    	   	   individual[i].bitString[j]=0;
  	    }
    }
    
    public void randomSelect(){
        for(int i=0;i<pop_size;i++){
    		for(int j=0;j<individual[0].size;j++)
    	   	   if(flip(0.5)) individual[i].bitString[j]=1;
    	   	    else individual[i].bitString[j]=0;
    	}
    }
    
    public void calculateFitness(){
    	globalFitness=0.0;
    	for (int i=0;i<pop_size;i++){
    		//individual[i].evaluate();
    		individual[i].fitness = eval.f(individual[i]); 
    	    globalFitness += individual[i].fitness;
        }     
   	}
   	
   	public void probabilities(){
    	double s=0.0;
    	for (int i=0;i<pop_size;i++)
    		individual[i].probSelect = (individual[i].fitness)/globalFitness; 
    	for(int i=0;i<pop_size;i++){
    	    s=s+individual[i].probSelect;
    	    individual[i].accumProb = s;
        }
    }
    
 	public Population select(){	
		Population newPop = new Population(pop_size,individual[0].size);
		double r;
	    int index=0;   
	    for(int i=0;i<pop_size ;i++){
	   	  r=Math.random();
	   	  //text.appendText("\n"+r);
	   	  for(int j=0;j<pop_size-1 ;j++){
	   	  	if(r<individual[0].accumProb){
	   	  		newPop.individual[i]=individual[0];
	   	  	    //text.appendText("\t"+i+"\t");
	   	  		break;
	   	  	}	
	   	  	if ((individual[j].accumProb<r) &&
	   	  	    (r<individual[j+1].accumProb)){
	   	  	   	 newPop.individual[i]=individual[j+1];
                //text.appendText("\t"+i+"\t"); 	   	  	    
 	   	  	    break;
	   	  	}
	   	    // to be sorted out later...
	   	    if (j==pop_size-2){
	   	    	 int pos = (int)(Math.random()*pop_size);
	   	    	 newPop.individual[i]=individual[pos];
	   	    }	 
	   	  }// for j
 	   	  newPop.individual[i].probSelect=0.0;
	   	  newPop.individual[i].accumProb =0.0;
	    }// for i
	    return newPop;
	} 
 
    // pm : probability of mutation
    public void mutate(double pm){
    	for(int i=0;i<pop_size;i++){
    		for( int j=0;j<individual[0].size;j++){
    			double r = Math.random();
    			if (r<pm){
    			   mutations++;	
    			   if(individual[i].bitString[j]==0)
    			       individual[i].bitString[j]=1;
    			   else individual[i].bitString[j]=0;
    			}
    		}
    	}
    }			       
    	
    // pc : probability of crossover
    public void crossover(double pc){
		int maxSize = (int)((pc+0.3)*pop_size);// 0.2 added for safety
		int[] index = new int[maxSize];
		int size    = 0; // will be set during crossover process
	    //selecting the condidates
		double r;
		for(int i=0;i<pop_size;i++){
			r=Math.random();
			if (r<pc){
				index[size]=i;
				size++;
			}
		}
		// even number has to be selected 0,1...,2k+1
		if (size%2==0){
			index[size]=(int)(Math.random()*pop_size);
			size++;
		}
		crossovers=size;
		// generating the crossover bit position
		int chromLength=individual[0].size;
		int pos = chromLength-1; //not allowed value
		while(pos==chromLength-1)
		  pos=(int)(Math.random()*chromLength);			
		// the crossover process
		for(int i=0;i<size-1;i+=2){
		  int chrom1 = index[i];
		  int chrom2 = index[i+1];
		  for(int j=pos+1;j<chromLength-1;j++){
		   int temp = individual[chrom1].bitString[j];
		   individual[chrom1].bitString[j]=individual[chrom2].bitString[j];
		   individual[chrom2].bitString[j]=temp;
		  }
		}
	}         
   		
    public boolean flip(double prob){
		boolean res;
	    if(Math.random()<=prob) res=true;
		                  else  res=false;
	    return res;
    }
    
    public void print(){
    	for(int i=0;i<individual.length;i++)
    	  System.out.println(individual[i]);
    }	  
}

class Eval{
	
	public double a;
	public double b;
	public double subdivisions;
	public double dx; 
	
	public Eval(double A, double B, double n){
		a = A;
		b = B;
	    subdivisions = n;
	    dx = (b-a)/subdivisions;	
	}
	
	public double f(Chromosome chrom){
		double s,x;
		double k;
		
		k = (double)chrom.decode();
		x =  a + k*dx;
		s = x*x*x - 60*x*x + 900*x + 100;
		return s;
	}
}