/*
 * @(#)Shadow.java 1.0 00/12/21
 *
 * You can modify the template of this file in the
 * directory ..\JCreator\Templates\Template_2\Project_Name.java
 *
 * You can also create your own project template by making a new
 * folder in the directory ..\JCreator\Template\. Use the other
 * templates as examples.
 *
 */

import java.awt.*;
import java.applet.*;


public class Shadow extends Applet
{
	private Fill fill;
	
	public void init()
	{
       Graphics p = getGraphics();
       fill = new Fill(p);
	}

	public void paint(Graphics g)
	{
      fill.start();	
	}

}

class Fill extends Thread {
	
	private Graphics graph;
	
	public Fill(Graphics g){
		graph=g;
	}
	
	public void run() {
		
		int ang=0;
		while(ang<=360){
		  graph.setColor(Color.white);
		  graph.fillArc(100,100,60,60,ang,180);	
          ang=ang+5;
          if (ang>=360) ang=0;
          graph.setColor(Color.blue);
		  graph.fillArc(100,100,60,60,ang,180);	
          try {
			 Thread.sleep(30);
		  } catch (InterruptedException e) {
			 System.err.println("Sleep Exception");
		  }	   
       } 
    }      
}