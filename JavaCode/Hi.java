/* this is an applet example */

import java.awt.*;
import java.applet.Applet;

public class Hi extends Applet {
 
  public void init() {
      repaint();
  } 
  
  public void paint(Graphics g) {
     
      g.drawString("This is great",50,50);
  }
}