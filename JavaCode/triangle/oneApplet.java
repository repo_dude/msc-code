/*
	Trivial applet that displays a string - 4/96 PNL
*/

import java.awt.*;
import java.applet.Applet;
import java.awt.event.*;

public class oneApplet extends Applet implements AdjustmentListener {
         
          private Scrollbar Horizontal;
          private Scrollbar Vertical;
          private int horz , vert ;
          private int xCoord,yCoord;  
          private float gradient;
 
          public void init() {
                    
                Horizontal = new Scrollbar(Scrollbar.HORIZONTAL);
                add(Horizontal);    
                Horizontal.addAdjustmentListener(this);      
                   
                Vertical = new Scrollbar(Scrollbar.HORIZONTAL);
                add(Vertical);    
                Vertical.addAdjustmentListener(this);
                
                horz=40;
                vert=40;
                Horizontal.setValue(40);
                Vertical.setValue(40); 
      
          }
          public void adjustmentValueChanged(AdjustmentEvent e) {
                
                horz=Horizontal.getValue();
                vert=Vertical.getValue();  
                //if (evt.getSource() == Horizontal) hChange();
                //if (evt.getSource() == Vertical) vChange();
                repaint();      
          }
          public void paint( Graphics g) {

                //horz=40;
                //vert=40;
                rTriangle(g,200,200);
                g.drawString("Horizontal value = "+horz,50,50);
                g.drawString("Vertical value   = "+vert,200,50);
                gradient=(float)vert/(float)horz;
                g.drawString("Gradient = "+gradient,50,100);
          }
          private void rTriangle(Graphics g,int xCoord,int yCoord) {

                g.drawLine(xCoord,yCoord,xCoord+horz,yCoord-vert);
                g.drawLine(xCoord+horz,yCoord,xCoord+horz,yCoord-vert);
                g.drawLine(xCoord,yCoord,xCoord+horz,yCoord); 
          }      
        
}






