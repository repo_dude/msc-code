// this application shows the use of the output stream.

import java.io.*;
import java.awt.*;
import java.awt.event.*;

public class prog2 extends Frame implements ActionListener,
              WindowListener {

        // data declareations

        private TextArea    inputText;
        private Label       file;
        private TextField   fileName;
        private BufferedReader inFile; // inFile is an input stream object
        private PrintWriter outFile;
        private MenuItem load,save,finish;

        public static void main(String[] args) {
                prog2 f = new prog2(); // instanciation of the application class prog
                f.setSize(300,300);
                f.display();
                f.setVisible(true);
        }


        public void display() {

               setLayout(new FlowLayout());

               MenuBar menuFileBar = new MenuBar();
               Menu aFileMenu = new Menu("File");

               load = new MenuItem("Open");
               aFileMenu.add(load);
               load.addActionListener(this);

               save = new MenuItem("Save");
               aFileMenu.add(save);
               save.addActionListener(this);

               finish = new MenuItem("exit");
               aFileMenu.add(finish);
               finish.addActionListener(this);


               menuFileBar.add(aFileMenu);
               setMenuBar(menuFileBar);
               file= new Label(" File :");
               add(file);

               fileName = new TextField(20);
               add(fileName);

               inputText = new TextArea(40,50);
               add("West",inputText);

               this.addWindowListener(this);
        }
        public void actionPerformed(ActionEvent evt)
        {
           if (evt.getSource()==finish) System.exit(0);
           if(evt.getSource()==load) {
              String name = fileName.getText();
              try {
                   inFile = new BufferedReader(
                                new FileReader(name));
                   inputText.setText("");
                   String line;
                   while((line=inFile.readLine())!=null)
                         inputText.append(line+"\n");
                   inFile.close();
              }
              catch (IOException e) {
                 System.err.println("File Error: "+ e.toString());
                 System.exit(1);
              }
           }
           if (evt.getSource()==save) {
               try{
                   outFile = new PrintWriter(
                                new FileWriter(fileName.getText(),true));
                   outFile.print(inputText.getText());
                   outFile.close();
               }
               catch (IOException e) {
                   System.err.println("File Error: "+ e.toString());
                   System.exit(1);
               }
           }
        }
        public void windowClosing(WindowEvent event){
                System.exit(0);
        }
        public void windowOpened(WindowEvent event) {
        }
        public void windowClosed(WindowEvent event) {
        }
        public void windowIconified(WindowEvent event) {
        }
        public void windowDeiconified(WindowEvent event) {
        }
        public void windowActivated(WindowEvent event) {
        }
        public void windowDeactivated(WindowEvent event) {
        }

}







