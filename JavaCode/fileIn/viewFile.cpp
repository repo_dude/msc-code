// this application shows the use of the output stream.

import java.io.*;
import java.awt.*;
import java.awt.event.*;

public class viewFile extends Frame implements ActionListener,
              WindowListener {

	// data declareations
        private TextArea    inputText;
        private Label       file;  
        private TextField   fileName;
        private Button      load;
        private BufferedReader inFile; // inFile is an input stream object
        
              
        public static void main(String[] args) {
		viewFile f = new viewFile(); // instanciation of the application class prog
                f.setSize(300,300);
                f.display();
                f.setVisible(true);
	}
                
     
        public void display() {
               
	       setLayout(new FlowLayout());

	       load = new Button("load");
	       add(load);
	       load.addActionListener(this);

	       file= new Label(" File :");
	       add(file);

	       fileName = new TextField(20);
	       add(fileName);

	       inputText = new TextArea(20,80);
	       add(inputText);

	       this.addWindowListener(this);
	}
	public void actionPerformed(ActionEvent evt)
	{
	   if(evt.getSource()==load) {
	    String name = fileName.getText();
	    try {
		 inFile = new BufferedReader(
				new FileReader(name));
		 inputText.setText("");
		 String line;
		 while((line=inFile.readLine())!=null)
			 inputText.append(line+"\n");
		 inFile.close();
	    }
	    catch (IOException e) {
                 System.err.println("File Error: "+ e.toString());
                 System.exit(1);
            }
	   }
        }
        public void windowClosing(WindowEvent event){
                System.exit(0);
        }        
        public void windowOpened(WindowEvent event) {
        }
        public void windowClosed(WindowEvent event) {
        } 
	public void windowIconified(WindowEvent event) {
        }
        public void windowDeiconified(WindowEvent event) {
        }
	public void windowActivated(WindowEvent event) {
        }
        public void windowDeactivated(WindowEvent event) {
        }

}          







