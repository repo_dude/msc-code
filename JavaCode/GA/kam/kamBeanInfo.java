/* 
	kamBeanInfo.java

	Author:			comp
	Description:	BeanInfo for class kam
*/

public class kamBeanInfo extends java.beans.SimpleBeanInfo
{

	// Generated BeanInfo just gives the bean its icons.
	// Small icon is in kam.gif
	// Large icon is in kamL.gif
	// It is expected that the contents of the icon files will be changed to suit your bean.

    public java.awt.Image getIcon(int iconKind)
	{
		java.awt.Image icon = null;
		switch (iconKind)
		{
			case ICON_COLOR_16x16:
	    		icon = loadImage("kam.gif");
				break;			
			case ICON_COLOR_32x32:
				icon = loadImage("kamL.gif");
			default:
				break;
		}
		return icon;
	}
}

/* kamBeanInfo.java */
