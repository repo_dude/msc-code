/*
 * GeneticAlgorithm.java 1.0 20/03/2001
 * 
 * Maximasing the function  x*sin(10*PI*x)+1.0 with x from [-1,2]
 *
 * K. Saadi 
 */
 
import java.awt.*;
import java.awt.event.*;

class GeneticAlgorithm extends Frame
{   

    public double pc=0.25; // probability of crossover;
    public double pm=0.01; // probability of mutation;
    public TextArea text;   
    
    
	public GeneticAlgorithm()
	{
		text = new TextArea(40,300);
		add(text);
		
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				dispose();
				System.exit(0);
			}
		});
	}
	
	public static void main(String args[])
	{   
		System.out.println("Starting GeneticAlgorithm...");
		GeneticAlgorithm ga = new GeneticAlgorithm();
		ga.setSize(650, 450);
		ga.setTitle("GeneticAlgorithm");
		ga.setVisible(true);
	    
	    Population pop    = new Population(30,22);
	    Population newPop = new Population(30,22);
	  	pop.randomSelect();
	  	pop.calculateFitness();
	    pop.probabilities();
        ga.display(pop);
	    newPop = ga.select(pop);
	   	newPop.calculateFitness();
	    newPop.probabilities();
	    ga.display(newPop);
     }
	
	public void display(Population pop){
	    text.appendText("\n  Chromosome"+"\t\t\t"+"fitness"
	                           +"\t\t\t"+"cummulative probability\n\n");
	    for(int i=0;i<pop.pop_size;i++)
	      text.appendText("  "+pop.individual[i]+"\t\t"
	                              +pop.individual[i].fitness+
	                           "\t\t"+pop.individual[i].accumProb+"\n"); 
	    text.appendText("\n  globalFitness ="+pop.globalFitness+"\n");
	   
	}
		 
	public Population select(Population pop){	
		Population newPop = new Population(pop.pop_size,pop.individual[0].size);
		double r;
	    int index=0;   
	    for(int i=0;i<pop.pop_size ;i++){
	   	  r=Math.random();
	   	  text.appendText("\n"+r);
	   	  for(int j=0;j<pop.pop_size-1 ;j++){
	   	  	if(r<pop.individual[0].accumProb){
	   	  		newPop.individual[i]=pop.individual[0];
	   	  	    text.appendText("\t"+i+"\t");
	   	  		break;
	   	  	}	
	   	  	
	   	  	if ((pop.individual[j].accumProb<r) &&
	   	  	    (r<pop.individual[j+1].accumProb)){
	   	  	   	 newPop.individual[i]=pop.individual[j+1];
                text.appendText("\t"+i+"\t"); 	   	  	    
 	   	  	    break;
	   	  	}
	   	    
	   	  }
	   	  newPop.individual[i].probSelect=0.0;
	   	  newPop.individual[i].accumProb =0.0;
	    }
	    return newPop;
	}          
}


class Chromosome{
	
	public int[]   bitString;
	public int     size;
    public double  fitness; // calculated outside the class
	public double  probSelect;
	public double  accumProb ;
	
	public Chromosome( int length){
		bitString   = new int[length];
		size        = length;
		probSelect  = 0.0;
		accumProb   = 0.0;
		fitness     = 0.0;
	}
	
	// decode from binary to decimal
	public int decode(){
	    int powerOf2=1;
	    int s = 0; 
	    for(int i = size-1;i>=0;i--){
	      s = s + bitString[i]*powerOf2;
	      powerOf2 *=2;
	    }
	    return s;
    }
    
    public String toString(){
    	String str="";
    	for(int i=0;i<bitString.length;i++)
    	 str = str + Integer.toString(bitString[i]);
        return str;
    } 
}

class Population{
   
    public   int           pop_size;
    public   Chromosome[]  individual;
    public   Eval          eval;
    public   double        globalFitness;
    public   double        searchSize;      
    
    public Population(int n,int m){
    	
    	pop_size    = n;
    	individual  = new  Chromosome[n];
    	searchSize  = Math.pow(2.0,(double)m);
    	eval        = new Eval(-1.0,2.0,searchSize);
    	
    	for(int i=0;i<n;i++){
    		individual[i]=new Chromosome(m);
    		for(int j=0;j<m;j++)
    	   	   individual[i].bitString[j]=0;
  	    }
    }
    
    public void randomSelect(){
        for(int i=0;i<pop_size;i++){
    		for(int j=0;j<individual[0].size;j++)
    	   	   if(flip(0.5)) individual[i].bitString[j]=1;
    	   	    else individual[i].bitString[j]=0;
    	}
    }
    
    public void calculateFitness(){
    	globalFitness=0.0;
    	for (int i=0;i<pop_size;i++){
    		//individual[i].evaluate();
    		individual[i].fitness = eval.f(individual[i]); 
    	    globalFitness += individual[i].fitness;
        }     
   	}
   	
   	public void probabilities(){
    	double s=0.0;
    	for (int i=0;i<pop_size;i++)
    		individual[i].probSelect = (individual[i].fitness)/globalFitness; 
    	 
    	for(int i=0;i<pop_size;i++){
    	    s=s+individual[i].probSelect;
    	    individual[i].accumProb = s;
        }
    }         
   		
    public boolean flip(double prob){
		boolean res;
	    if(Math.random()<=prob) res=true;
		                  else  res=false;
	    return res;
    }
    
    public void print(){
    	for(int i=0;i<individual.length;i++)
    	  System.out.println(individual[i]);
    }	  
}
    

class Eval{
	
	public double a;
	public double b;
	public double subdivisions;
	public double dx; 
	
	public Eval(double A, double B, double n){
		a = A;
		b = B;
	    subdivisions = n;
	    dx = (b-a)/subdivisions;	
	}
	
	public double f(Chromosome chrom){
		double s,x;
		double k;
		
		k = (double)chrom.decode();
		x =  a + k*dx;
		s =  x*Math.sin(10*Math.PI*x)+1.0;
		return Math.abs(s);
	}
}			
    









