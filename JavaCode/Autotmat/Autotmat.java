
import java.awt.*;
import java.applet.*;
import java.awt.event.*;


public class Autotmat extends Applet implements ActionListener
{
	public int[][]   mesh;
	public Button    iterate;
	
	public void init()
	{
       mesh = new int[40][40];
       iterate = new Button("next iteration");
       add(iterate);
       iterate.addActionListener(this);
       
       for(int i=0;i<mesh.length;i++)
        for(int j=0;j<mesh[0].length;j++)
          mesh[i][j]=0;
       squareConfig();     
	}
	
	public void squareConfig(){
		for(int i=10;i<30;i++)
		 for(int j=10;j<30;j++)
		 {
		   if ((i-20)*(i-20)+(j-20)*(j-20)<=64)
		   mesh[i][j]=1;
		 }  
	}
	
	public void runAutomaton(){
		for(int i=0; i<mesh.length;i++)
		  for(int j=0;j<mesh[0].length;j++){
		  	  if (mesh[i][j]==1) {
		  	  	  mesh[i][j]=0;
		  	  	  int rand = (int)(Math.random()*4);
		  	  	  switch(rand){
		  	  	  	case 0 : if (allowed(i+1,j))
		  	  	  		          if (mesh[i+1][j]==0) mesh[i+1][j]=1;
			                 break; 
		  	  	  	case 1 : if (allowed(i-1,j))
		  	  	  		          if (mesh[i-1][j]==0) mesh[i-1][j]=1;
			                 break; 
		  	  	  	case 2 : if (allowed(i,j+1))
		  	  	  		          if (mesh[i][j+1]==0) mesh[i][j+1]=1;
			                 break; 
		  	  	  	case 3 : if (allowed(i,j-1))
		  	  	  		          if (mesh[i][j-1]==0) mesh[i][j-1]=1;
			                 break; 
			      }
			   }
			}
	}		                 

			
		
    public boolean allowed(int i, int j){
    	boolean result =false;
    	if (i<mesh.length && i>=0 && j<mesh[0].length && j>=0)
    	  result=true;
    	return result;
    }	  
    	
    public void actionPerformed(ActionEvent evt){
    	if (evt.getSource()==iterate) {
    		runAutomaton();
    	}
    	repaint();
    }		
    	
	
	public void paint(Graphics g)
	{
		for(int i=0;i<mesh.length;i++)
			for(int j=0;j<mesh[0].length;j++){
				if (mesh[i][j]==1)
					g.fillOval(50+5*i,50+5*j,5,5);
				else
				    g.drawOval(50+5*i,50+5*j,5,5);
			}	    
				
		//g.drawString("Welcome to Java!!", 50, 60 );
	}

}
