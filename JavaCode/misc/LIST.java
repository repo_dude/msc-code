/*
	WEEK4 EXAMPLE 3
	Player Class

*/


public class Player
{
	private String name;
	private int number;
	
	public Player()
	{
		name="";
		number=0;
	}
	public Player(String newName, int newNumber)
	{
		name=newName;
		number=newNumber;
	}
	public String toString()
	{
		return name +"\t"+number ;
	}
}

