
import java.awt.*;
import java.applet.*;
import java.awt.event.*; 



public class Solar extends Applet implements ActionListener
{
	private Spin   spin;
	private Button start;
	
	public void init()
	{	
		start = new Button("Start");
		add(start);
		start.addActionListener(this);
       
	}
	
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource()==start) {
			Graphics g = getGraphics();
			g.setColor(Color.yellow);
			g.fillOval(200,200,30,30);
			Lunar  spin1 = new Lunar(g,200,200,100,10,1);
			Spin  spin  = new Spin(g,200,200,50,10,5);
			spin.start();
			spin1.start();
		}
	}	


}

class Spin extends Thread{
	
	protected  int       xCentre;
	protected  int       yCentre;
	protected  int       radius;
	protected  int       diameter;
	protected  double       pi=3.1415;
	protected  Graphics  g;
	protected  int       c;
	protected  double    t;
	protected  int       dt;
	
	public Spin(Graphics grph, int x0,int y0,int r, int d0,int cte) {
		g=grph;
		xCentre  = x0;
		yCentre  = y0;
		radius   = r;
		diameter = d0;
		c = cte;
		dt=50;
    }		
	
	public int getX() { return xCentre; }
	
	public int getY() { return yCentre; }
	
	public void setX(int x) {
		xCentre=x;
	}
	
	public void setY(int y) {
		yCentre = y;
	}		 
	
    public void turn(){
			g.setColor(Color.white);
			g.fillOval(xCentre+(int)(radius*Math.cos(c*t)),
			           yCentre+(int)(radius*Math.sin(c*t)),diameter,diameter);
			t+=0.05;
			
			g.setColor(Color.red);
			g.fillOval(xCentre+(int)(radius*Math.cos(c*t)),
			           yCentre+(int)(radius*Math.sin(c*t)),diameter,diameter);
			if (t>2*pi) t=0;
	}		

	public void run() {
		double t=0; 
		while(t<=2*pi) {	
			turn();
			try {
				Thread.sleep(dt);
			} catch (InterruptedException e) {
				System.err.println("Sleep Exception");
			}	
	    }
	}
}

class Lunar extends Spin {	
	
	private   int  relativeRadius=20;
	private   int  relativeX;
	private   int  relativeY; 
	private   int  k;
	public Lunar(Graphics grph, int x0,int y0,int r, int d0,int cte) {
		
		super(grph, x0,y0,r, d0,cte);
     k=7;
    }
 
    
    public void turn(){
			g.setColor(Color.white);
			g.fillOval(xCentre+(int)(radius*Math.cos(c*t)),
			           yCentre+(int)(radius*Math.sin(c*t)),diameter,diameter);
			g.fillOval(xCentre+(int)(radius*Math.cos(c*t))+(int)(relativeRadius*Math.cos(k*t)),
		               xCentre+(int)(radius*Math.sin(c*t))+(int)(relativeRadius*Math.sin(k*t)),
		               5,5); 	
			
			t+=0.05;
			
			g.setColor(Color.red);
			g.fillOval(xCentre+(int)(radius*Math.cos(c*t)),
			           yCentre+(int)(radius*Math.sin(c*t)),diameter,diameter);
			g.setColor(Color.blue);
			g.fillOval(xCentre+(int)(radius*Math.cos(c*t))+(int)(relativeRadius*Math.cos(k*t)),
		               xCentre+(int)(radius*Math.sin(c*t))+(int)(relativeRadius*Math.sin(k*t)),
		               5,5); 	
		
			
			if (t>2*pi) t=0;
    }
}   	 