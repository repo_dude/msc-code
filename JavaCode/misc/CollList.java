import java.text.*;

public class CollList{

      private double totalValue;
      private CDNode head;
      private CDNode browse;
      private int    length=0;

      public CollList() {
      }
      // in order to maintain a list we always need to know it's head
      // head is a reference to the first item in the list
      //**** Creating the list ****//
      public void addCD(String artist,String title,double cost, int tracks){

	 CD aCD = new CD(artist,title, cost, tracks);
	 if (length==0) {
	     head  = new CDNode(aCD);
	     browse=head;
	 }
	 else {
	    CDNode temp= new CDNode(aCD);
	    browse.next=temp;
	    browse=temp; // browse is a reference to the last created item
	 }
	 length++;
	 totalValue+=cost;
      }

      /*---------------------- reading the list -------------------------*/
      public String toString(){
	 String report="";
	 browse=head;
	 while(browse!=null){
	     report += browse.theCD.toString()+"\n";
	     browse=browse.next;
	 }
	 return report;
      }

      /*------------------------------------------------------------------*/
      /*--------------------- inner classes ------------------------------*/
      /*------------------------------------------------------------------*/

      public class CDNode {
	    public CD     theCD;
	    public CDNode next;

	    public CDNode(CD newCD){
		  theCD = newCD;
		  next  = null;
	    }
      }

      public class CD {

	    private String  artist;
	    private String  title;
	    private double  cost;
	    private int     tracks;

	    public CD(String xArtist,String xTitle,double xCost, int xTracks)
	    {
		artist = xArtist;
		title  = xTitle;
		cost   = xCost;
		tracks = xTracks;
	    }

	    public String toString() {
		String out;
		out= artist+"\t"+title+"\t�"+cost+"\t"+tracks;
		return out;
	    }

      }
}



