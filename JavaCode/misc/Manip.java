import java.io.*;

public class Manip {
    public static void main(String[] args) throws IOException {
        File inputFile = new File("farrago.txt");
        File outputFile = new File("outagain.txt");

        FileInputStream in = new FileInputStream(inputFile);
        FileOutputStream out = new FileOutputStream(outputFile);
        
        int c;
        String line;
        String res="";
        while ((line = in.readLine()) != null)
         res=res+line+" ";          
          
        out.write(line);

        in.close();
        out.close();
    }
}
