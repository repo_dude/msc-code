import java.text.*;

public class CDCollection {

      private CD[]    collection;
      private int     currentSize;
      private int     maxSize;
      private double  totalValue;

      public CDCollection() {
	   currentSize=0;
	   maxSize=100;
	   collection = new CD[maxSize];
	   totalValue=0.0;
      }

      public void addCD(String artist, String title, double cost, int n){
	   collection[currentSize]=new CD(artist,title,cost,n);
	   currentSize++;
	   totalValue+=cost;
      }

      public String toString(){
	   String report="";
	   for(int i=0;i<currentSize;i++)
	       report+=collection[i].toString()+"\n";
	   return report;
      }
      // **************************************************************
      //  Class CD
      // **************************************************************

      public class CD {
	  private String title;
	  private String artist;
	  private double cost;
	  private int    tracks;

	  public CD(String singer, String name, double theCost, int n){
	       title  = name;
	       artist = singer;
	       cost   = theCost;
	       tracks = n;
	  }

	  public String toString(){
	       String description;
	       description = artist+"\t"+title+"\t�"+cost+"\n"+tracks;
	       return description;
	  }
      }
}