// Example of StringTokenizer class                  K. Saadi
import java.io.*;
import java.util.*;

class TokenF {
     public static void main( String[] args) throws IOException {

	FileReader input = new FileReader("cities.dat");
	BufferedReader inFile = new BufferedReader(input);
	String line;

	while((line=inFile.readLine())!=null) {
	   StringTokenizer data = new StringTokenizer(line," ");
	   while(data.hasMoreTokens())
	       System.out.println( data.nextToken() );
	}
	inFile.close();
     }
}