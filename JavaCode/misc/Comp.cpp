////////////////////////////////////////////////////////////////////////
//    using compareTo methode to sort an array of strings.            //
//    Author : Kamel SAADI                                            //
///////////////////////////////////////////////////////////////////////
public class Comp{
     public static void main (String[] args){
       String[] a={"saadi","ad","bcc","bbc","aaa"};
       String temp;
       for(int i=0;i<a.length;i++) System.out.println(a[i]);
       for(int i=0;i<a.length-1;i++)
	for(int j=i+1;j<a.length;j++)
	{
	  int n=a[i].compareTo(a[j]);
	  if (n>0) {
	    temp=a[i];
	    a[i]=a[j];
	    a[j]=temp;
	  }
	}
       // after Sorting
       System.out.println("\n");
       for(int i=0;i<a.length;i++) System.out.println(a[i]);
     }
}
