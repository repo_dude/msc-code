import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

public class mouse extends Applet implements MouseListener{

    private int whereX=0;
    private int whereY=0;

    public void init() {
	  addMouseListener(this);
    }
    public void paint(Graphics g) {
	 g.drawRect(50,50,100,100);
	 if (whereX<=150 && whereX>=50 && whereY<=150 && whereY>=50)
	  g.drawString("I'm inside",whereX,whereY);
	 else
	  g.drawString("I'm outside",whereX,whereY);
    }

    // Mouse events

    public void mouseClicked(MouseEvent evt) {
	 whereX=evt.getX();
	 whereY=evt.getY();
	 repaint();
    }
    public void mousePressed(MouseEvent evt) {
    }
    public void mouseEntered(MouseEvent evt) {
    }
    public void mouseExited(MouseEvent evt) {
    }
    public void mouseReleased(MouseEvent evt) {
    }

}