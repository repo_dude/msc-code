
/*
   This program shows how to process an input file with specific commands ( testin.out )
   and write the results to an output file ( testout.txt ).
   
   K. Saadi
*/


import java.io.*;

class Prompt {
     
     static DataInputStream  cin;
     
     public static void main(String[] args) throws IOException {
     
          System.setIn(new BufferedInputStream(new FileInputStream("testin.txt"))); 
          System.setOut(new PrintStream(new FileOutputStream("testout.txt")));
          cin = new DataInputStream(System.in);
          char car='e';
          while( car!='x') {
              switch(car) {
                  case 'f'  : System.out.println(" First is selected");
                            break;
                  case 's' : System.out.println(" Second is selected");
                            break;
                  case ' ' : 
                            break;
                  default : System.out.println("No selection");
                            break;
              }
              car=(char)cin.read();                                                  
         }        
     }
 }         
          
            
