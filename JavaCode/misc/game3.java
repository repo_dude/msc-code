

// java application template.

import java.awt.*;
import java.awt.event.*;
import java.applet.Applet;
import javax.swing.*;

public class game3 extends Applet
		   implements MouseListener,ActionListener {

	//private Point     clickPoint=null;
	private boolean   touched=false;
	private Ball      ball;
	private int       currentX;
	private int       currentY;
	public  Timer     timer,timer2;
	private int       count=0;

	public void init() {
		ball =new Ball();
		timer= new Timer(1500,this);
		//timer2= new Timer(500,this);
		this.addMouseListener(this);
	}


	public void paint(Graphics g){

	     ball.show(g);
	     timer.start();
	     if ( (currentX>=ball.x)
		   && ( currentX<=(ball.x + ball.diameter) )
		   && ( currentY>=ball.y )
		   && ( currentY<=(ball.y + ball.diameter) ) ) {
		  count++;
		  g.setColor(Color.black);
		  g.drawString("got me",currentX,currentY);
		  //repaint();
		  //timer2.start();
	     }
	     System.out.println(ball.x+" "+ball.y+" "+currentX+" "+currentY);
	     g.setColor(Color.black);
	     g.drawString("Count ="+ count,100,350);
	     //ball.hide(g);

	}

	public void actionPerformed(ActionEvent event) {
	      // repaint();
	}

	public void mouseClicked(MouseEvent event) {
	       currentX=event.getX();
	       currentY=event.getY();
	       repaint();
	}

	public void mouseReleased(MouseEvent evt) {
	}

	public void mousePressed(MouseEvent evt) {
		  //repaint();
	}

	public void mouseEntered(MouseEvent evt) {
	}

	public void mouseExited(MouseEvent evt) {
	}
}

class Ball {
      public int diameter =10;
      public int x,y;

      public Ball(){
      }

      public void show(Graphics g){
	    g.setColor(Color.black);
	    g.drawRect(0,0,300,300);
	    x =  (int)( Math.random()*280 )+1;
	    y  =  (int)( Math.random()*280 )+1;
	    g.setColor(Color.red);
	    g.fillOval(x,y,diameter,diameter);
      }
      public void hide(Graphics g){
	    g.setColor(Color.white);
	    g.fillOval(x,y,diameter,diameter);
      }


}




