// Example of using interfaces.

interface Identifier {
     public void iam();
     public void repeat();
}

class Horse implements Identifier {
     private String myNature;
     private int legs=4;

     public Horse(){
	myNature="Horse";
     }
     public void iam(){
	System.out.println("I am a "+myNature);
     }
     public void repeat(){
	System.out.println("It is right... I am a Horse");
     }
     // you could add other methods here.
}

class Bird implements Identifier {
    public void iam(){
       System.out.println("I am a Bird");
    }
    public void repeat(){
       System.out.println("I am a Bird, I don't have to say it again");
    }
}

class WhoIs{

   public static void main(String[] args) {

     Identifier current = new Horse();
     current.iam();
     current.repeat();

     current = new Bird();
     current.iam();
     current.repeat();

     ( (Horse) current).iam();
   }
}

