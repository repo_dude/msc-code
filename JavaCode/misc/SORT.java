import java.text.*;

public class CollList{

      private double totalValue;
      private CDNode head;
      private CDNode browse;
      private int    length=0;

      public CollList() {
      }
      // in order to maintain a list we always need to know it's head
      // head is a reference to the first item in the list
      //**** Creating the list ****//
      public void addCD(String artist,String title,double cost, int tracks){

	 CDNode pos,lastPos;
	 CD aCD = new CD(artist,title, cost, tracks);
	 CDNode temp= new CDNode(aCD);

	 if (length==0) {
	     head  = temp;
	     browse= head;
	 }
	 else {
	    pos=head;
	    while(pos!=null){
	       int n = pos.theCD.artist.compareTo(artist);
	       if (pos.next==null) int l=1;
		 else l = pos.next.theCD.artist.compareTo(artist);
	       if (n<=0 && l>=0) {
		 insert(pos,temp);
	       }
	       if (n>0) insert(head,temp);


	    browse.next=temp;
	    browse=temp; // browse is a reference to the last created item
	 }
	 length++;
	 totalValue+=cost;
      }

      public void insert(CDNode nodeA,CDNode nodeB) {
	   if (nodeA==head) {
		 nodeB.next=head;
		 head=nodeB;
	   }
	   else if(nodeA.next==null) nodeA.next=nodeB;
		else {
		  nodeB.next=nodeA.next;
		  nodeA.next =nodeB;
		}
      }





      /*---------------------- reading the list -------------------------*/
      public String toString(){
	 String report="";
	 browse=head;
	 while(browse!=null){
	     report += browse.theCD.toString()+"\n";
	     browse=browse.next;
	 }
	 return report;
      }

      /*------------------------------------------------------------------*/
      /*--------------------- inner classes ------------------------------*/
      /*------------------------------------------------------------------*/

      public class CDNode {
	    public CD     theCD;
	    public CDNode next;

	    public CDNode(CD newCD){
		  theCD = newCD;
		  next  = null;
	    }
      }

      public class CD {

	    private String  artist;
	    private String  title;
	    private double  cost;
	    private int     tracks;

	    public CD(String xArtist,String xTitle,double xCost, int xTracks)
	    {
		artist = xArtist;
		title  = xTitle;
		cost   = xCost;
		tracks = xTracks;
	    }

	    public String toString() {
		String out;
		out= artist+"\t"+title+"\t�"+cost+"\t"+tracks;
		return out;
	    }

      }
}



