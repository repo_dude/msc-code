// Example of StringTokenizer class                  K. Saadi

import java.util.*;

class Token {
     public static void main( String[] args) {

	String example=" my name   is  13   456     890 ";
	StringTokenizer data = new StringTokenizer(example," ");
	while(data.hasMoreTokens()) {
	    System.out.println( data.nextToken() );
	}
     }
}