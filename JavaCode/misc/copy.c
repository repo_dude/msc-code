import java.io.*;

class Copy {
       public static void main( String[] args ) {
	  try {
		FileInputStream  sourceFile = new FileInputStream(args[0]);
		FileOutputStream destFile   = new FileOutputStream(args[1]);
		boolean eof = false;
		int count = 0;
		while(!eof) {
		    int input = sourceFile.read();
		    destFile.write(input);
		    if (input == -1) eof=true;
		     else count++;
		}
		sourceFile.close();
		destFile.close();
		System.out.println("\nBytes copied: "+count);
	  }
	  catch (IOException e) {
		System.out.println("Usage : copy file1 file2");
		System.out.println("Error -- "+e.toString());
	  }
       }
}

