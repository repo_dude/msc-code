import java.awt.*;
import java.applet.Applet;
import java.awt.event.*;

public class oneApplet extends Applet 
            implements ActionListener,MouseListener
{
       private Table sales;
       private vector colSums;
       private vector rowSums;
       private TextField value;
       private int temp,i,j;
       
       public void init() {
               sales = new Table(4,7);
               rowSums = new vector(4,1);
               colSums = new vector(1,7);
               Label l = new Label("Enter number and click component");
               add(l);
               value = new TextField(8);
               add(value);
               value.addActionListener(this);
               this.addMouseListener(this);
       }
       public void paint(Graphics g) {
               sales.startPoint(60,60);
               rowSums.startPoint(360,60);
               colSums.startPoint(60,160);
                 
               sales.display(g);
               for( i=0;i<4;i++) {
                   temp=0;
                   for(j=0;j<7;j++) 
                       temp +=sales.getValue(i,j);
                   rowSums.setValue(i,1,temp);              
               }   
               for( j=0;j<7;j++) {
                   temp=0;
                   for(i=0;i<4;i++) 
                       temp +=sales.getValue(i,j);
                   rowSums.setValue(1,j,temp);              
               }        

               colSums.display(g);
               rowSums.display(g);
               g.drawString("Total is " + sales.sum, 100, 200);

       }
       public void actionPerformed(ActionEvent event) {
               int newValue = Integer.parseInt(value.getText());
               sales.setValue(newValue);
               repaint();
       }        
       public void mouseClicked(MouseEvent event) {
               int x = event.getX();
               int y = event.getY();
               sales.selectComponent(x, y);
               repaint();
       }
       public void mouseReleased(MouseEvent e) {
       }        
       public void mousePressed(MouseEvent e) {
       }
       public void mouseEntered(MouseEvent e) {
       }
       public void mouseExited(MouseEvent e) {
       }


public class Table {
        
        protected int[][] data;
        protected int rowIndex;
        protected int colIndex;
        protected int newValue;
        
        public int sum;
        
        protected int xStart ;
        protected int yStart ;
        protected final int boxHeight = 20;
        protected final int boxWidth = 40;
        
        public Table(int rows, int columns) {
                data = new int[rows][columns];
                
                for(int row=0; row<rows; row++)
                   for(int col=0; col<columns;col++)
                      data[row][col]=0;
        }
        public void startPoint(int xCoord, int yCoord) {
                xStart=xCoord;
                yStart=yCoord;
        }        
        public int getValue(int i,int j) {
                return data[i][j];
        }        
        public void display(Graphics g) {
                for(int row=0;row<data.length; row++)
                   for(int col=0;col<data[0].length;col++) {
                       int x = xStart+col*boxWidth;
                       int y = yStart+row*boxHeight;
                       g.drawRect(x,y,boxWidth,boxHeight);
                       g.drawString(Integer.toString(data[row][col]),
                                     x,y + 3*boxHeight/4);
                    }
                    calculateSum();
        }
        public void selectComponent(int x, int y) {
                rowIndex = (y-yStart)/boxHeight;
                colIndex = (x-xStart)/boxWidth;
                data[rowIndex][colIndex] = newValue;
        }
        public void setValue(int value) {
                newValue=value;
        }
        private void calculateSum() {
                sum=0;
                for(int row = 0; row<data.length; row++)
                   for( int col = 0; col < data[0].length; col++)
                      sum += data[row][col];
        }
}
public class vector extends Table {
        
        public vector(int rows,int cols) {
                super(rows,cols);
        }         
        public void selectComponent() {
        }
        public void setValue(int i,int j,int theValue) {
                data[i][j]=theValue;
        }        

}
        
        
        
        
        
        
        
}             
                   
              

