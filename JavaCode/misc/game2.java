

// java application template.

import java.awt.*;
import java.awt.event.*;
import java.applet.Applet;

public class game2 extends Applet implements MouseListener,ActionListener {

	private Point     clickPoint=null;
	private boolean   touched;
	private Ball      ball;
	private int       currentX;
	private int       currentY;

	public void init() {
		ball =new Ball();
		this.addMouseListener(this);
	}

	public void paint(Graphics g){
	     //for(int i=1;i<=1000;i++) {
	     ball.start(g);
	     if(clickPoint!=null) {
	       currentX=clickPoint.x;
	       currentY=clickPoint.y;
	       /*if ( currentX>=ball.getX()
		   && currentX<=ball.getX()+ball.getDiameter()
		   && currentY>=ball.getY()
		   && currentY<=ball.getY()+ball.getDiameter() )
	       */
	       g.drawString("got me",currentX,currentY);
	     }
	    //}
	}

	public void actionPerformed(ActionEvent event) {
	       //repaint();
	}

	public void mouseClicked(MouseEvent event) {
	       clickPoint=event.getPoint();
	       repaint();
	}

	public void mouseReleased(MouseEvent evt) {
	}

	public void mousePressed(MouseEvent evt) {
		  //repaint();
	}

	public void mouseEntered(MouseEvent evt) {
	}

	public void mouseExited(MouseEvent evt) {
	}
}

class Ball extends Thread {
      private int diameter =10;
      private int leftX,rightX;
      private int topY=0,botomY=300;

      public Ball(){
      }
      public int getX(){ return leftX; }
      public int getY(){ return topY ; }
      public int getDiameter(){ return diameter; }

      public void run(Graphics g){
	    g.setColor(Color.black);
	    g.drawRect(leftX,topY,300,300);
	    leftX =  (int)( Math.random()*280 )+1;
	    topY  =  (int)( Math.random()*280 )+1;
	    g.setColor(Color.red);
	    g.fillOval(leftX,topY,diameter,diameter);
	    try{
	       Thread.sleep(2000);
	    }
	    catch (InterruptedException e) {
	       System.out.println("Sleep exception");
	    }
	    g.setColor(Color.white);
	    g.fillOval(leftX,topY,diameter,diameter);
      }

}




