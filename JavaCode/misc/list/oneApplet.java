import java.awt.*;
import java.applet.Applet;

public class oneApplet extends Applet {

  Choice micro;
  List parts;
  Button done;
  TextArea spec;

  int[]    procType = new int[4];
  float[]  procCost = new float[4];
  int p=0;
  String[] peripheral = new String[8];

  public void init() {
    
    micro = new Choice();
    micro.addItem("P133");    
    micro.addItem("P150");
    micro.addItem("P166");
    micro.addItem("P200");
    add(micro);
    
    procType[0]=133; procCost[0]=85;
    procType[1]=150; procCost[1]=95;
    procType[2]=166; procCost[2]=135;
    procType[3]=200; procCost[3]=165;

    peripheral[0]="CD-ROM";
    peripheral[1]="Sound card";
    peripheral[2]="Speakers";
    peripheral[3]="Tape Drive";
    peripheral[4]="Zip Drive";
    peripheral[5]="Modem";
    peripheral[6]="Extra 16Mb RAM";
    peripheral[7]="1.2Gb Hard Drive";
    
    parts = new List(6,true);
    for(int loop=0; loop <=7; loop++)
     parts.addItem(peripheral[loop]); 
    add(parts);

    done = new Button("Done");
    add(done);

    spec = new TextArea(10,30);
    add(spec);

    validate();
  }
  public boolean action(Event evt, Object arg)
  {
    p=micro.getSelectedIndex();
    if ( evt.target==done ) {
      spec.setText("Pentium" + procType[p] + " at �"+procCost[p]+"\n");
      for(int loop = 0; loop<=7;loop++)
        if(parts.isIndexSelected(loop) == true)
          spec.append(peripheral[loop]+"\n");
    }   
    return true;
  }
} 
    










