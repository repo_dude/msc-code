@echo off
cls
if exist %1.class del %1.class
@echo compiling...
javac %1.java
if not exist %1.class goto failed
cls
@echo running...
@echo.
java %1 %2 %3 %4 %5 %6 %7 %8 %9 %10
@echo.
pause
goto done
:failed
@echo compile failed.
:done

