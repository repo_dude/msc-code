public class Simulator
{
	short [] MEM;
	short DATA_REGISTER;
	short INPUT_BUFFER;
	short OUTPUT_BUFFER;
	
	
	public static void main (String[] args){
		Simulator s = new Simulator ();
	}
	
	public Simulator(){
		MEM = new short [0x400];
		MEM[0x080-1]=(short)(0x0100);
		MEM[0x081-1]=(short)(0x2101);
		MEM[0x082-1]=(short)(0x100A);
		MEM[0x083-1]=(short)(0x9000);
		MEM[0x084-1]=(short)(0x4000);
		
		MEM[0x100-1]=400;
		MEM[0x101-1]=200;
		
		INPUT_BUFFER = 0x0009;
		OUTPUT_BUFFER = 0x000A;
		
		short OP_CODE;
		short ADDRESS;
		for(int j=0x0080;j<0x0085;j++){
			OP_CODE=(short)(0x000F&(MEM[j-1]>>12));
			switch (OP_CODE){
				case 0x0: ADDRESS =(short)(0x01FF&MEM[j-1]);
						  DATA_REGISTER = MEM[ADDRESS-1];
						  System.out.print ("MOVXA ");
						  System.out.println (ADDRESS);
						  break;
				case 0x1: ADDRESS =(short)(0x01FF&MEM[j-1]);
						  MEM[ADDRESS-1] = DATA_REGISTER;
						  System.out.println ("MOVAX");
						  break;
				case 0x2: ADDRESS =(short)(0x01FF&MEM[j-1]);
						  DATA_REGISTER = (short)(DATA_REGISTER+MEM[ADDRESS-1]);
						  System.out.print ("ADDXA ");
						  System.out.println (ADDRESS);
						  break;
				case 0x3: System.out.println ("");
						  break;
				case 0x4: System.out.println ("HALTM");
						  System.exit (0);
						  break;
				case 0x5: System.out.println ("");
						  break;
				case 0x6: System.out.println ("");
						  break;
				case 0x7: System.out.println ("");
						  break;
				case 0x8: System.out.println ("");
						  break;
				case 0x9: System.out.print ("WAOUT ");
						  System.out.println(MEM[OUTPUT_BUFFER-1]);
						  break;
				case 0xA: System.out.println ("");
						  break;
				case 0xB: System.out.println ("");
						  break;
				case 0xC: System.out.println ("");
						  break;
				case 0xD: System.out.println ("");
						  break;
				case 0xE: System.out.println ("");
						  break;
				case 0xF: System.out.println ("");
						  break;
			}
		}
	}
}



