// {$R San.JFM}

import java.awt.*;

// Class San
public class San extends Frame
{
    final int MenuBarHeight = 0;

    // Component Declaration
    public TextArea TextArea1;
    // End of Component Declaration

    // Constructor
    public San()
    {
        // Frame Initialization
        setForeground(Color.black);
        setBackground(Color.lightGray);
        setFont(new Font("Dialog",Font.BOLD,12));
        setTitle("noname01");
        setLayout(null);
        // End of Frame Initialization

        // Component Initialization
        TextArea1 = new TextArea("TextArea1");
        TextArea1.setForeground(Color.black);
        TextArea1.setBackground(Color.white);
        TextArea1.setFont(new Font("Dialog",Font.BOLD,12));
        // End of Component Initialization

        // Add()s
        add(TextArea1);
        // End of Add()s

        InitialPositionSet();
    }

    public void InitialPositionSet()
    {
        // InitialPositionSet()
        reshape(144,126,400,300);
        TextArea1.reshape(94,42+MenuBarHeight,292,214);
        // End of InitialPositionSet()
    }

    public boolean handleEvent(Event evt)
    {
        // handleEvent()
        if (evt.id == Event.WINDOW_DESTROY && evt.target == this) San_WindowDestroy(evt.target);
        // End of handleEvent()

        return super.handleEvent(evt);
    }

    public void paint(Graphics g)
    {
        // paint()
        // End of paint()
    }

   // main()
   public static void main(String args[])
   {
       San San = new San();
       San.show();
   } // End of main()

    // Event Handling Routines
    public void San_WindowDestroy(Object target)
    {
        System.exit(0);
    }

    // End of Event Handling Routines

} // End of Class San
