import gui.*;
import java.io.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;

public class Browser extends 
    ImageFrame implements ActionListener
    {
  
	private  String    path;
	//private  Image     image; image is inherited from ImageFrame 
	private  Button    back; 
	private  Button    next;
	private  String[]  images;
	
	private  File      myDirectory; 
	private  int       index=0;
	private  String    imageFile;
	private  Label     location;
	private  Button    set;
	private  boolean   loaded;
	private  Vector    imVector;
	private  Button    rand;
	private  Image[]   imgs;
	private  Image     xImage;  
	
	public Browser()
	{
	
	      super("Tiny Image Browser");
	      setBackground(Color.gray);
	      setLayout(new FlowLayout() );
	      	      
	      back = new Button("<- back ");
	      next = new Button(" next ->");
	      add(back);
	      add(next);
          location = new Label("   Location: ");
          add(location);
          
          set = new Button("Set Image Database");
          add(set);
          
          rand = new Button(" Random Display ");
          add(rand);
          
          loaded=false;
          index=0;
          
          next.addActionListener(this);
          back.addActionListener(this);
          set.addActionListener(this);
          rand.addActionListener(this);
                
    }      	
	
	
	
	
	
	public static void main(String args[]) {
	     Browser bs = new Browser();
	     bs.setSize(600,500);
	     bs.setVisible(true);
	}

    /*public void paint(Graphics screen) {
        if(loaded){ 
         //imageFile=(String)imVector.elementAt(index);
         //openGif(path+imageFile);
         //Toolkit toolkit = Toolkit.getDefaultToolkit();
         //image = toolkit.getImage(path+imageFile);
         boolean b= screen.drawImage(image,10,80,350,300,this);
        } 
    }*/       
    
    private Frame f = new Frame();	
	
	public void setDirectory() {
	     FileDialog fd = new FileDialog(f,"Images");
	     fd.show();
	     path=fd.getDirectory();
	     fd.dispose();
	     if (path!=null) load();
	}       
	      
	
	public void load() {
	     
	     imVector= new Vector();
	     myDirectory = new File(path);
         boolean checkDirectory = myDirectory.isDirectory();
         
         if(checkDirectory){
             
             loaded=true;  
             String[] temp=myDirectory.list();
             // filtering files: loading only the image file names.        
             int k=0;
             for(int i=0; i<=temp.length-1;i++) 
              if ((temp[i].toUpperCase().indexOf(".GIF",0)>0) || 
               (temp[i].toUpperCase().indexOf(".JPEG",0)>0) || 
               (temp[i].toUpperCase().indexOf(".JPG",0)>0)  ) {
                    imVector.addElement(temp[i]);    
                    
              }
              openGif(path+(String)imVector.elementAt(index));     
        
               
          } 
          
                 //images=temp;
            
    }
     
    public void next() {
         
         String fn;
         index++;
         if (index==imVector.size()) index=0;
         fn=(String)imVector.elementAt(index);
         openGif(path+fn);     
    }     
             
    public void back() {
         
         String fn;
         index--;
         if (index<0) index=imVector.size()-1;
         fn= (String)imVector.elementAt(index); 
         openGif(path+fn);
    }     
    
	
	public void actionPerformed (ActionEvent evt ) {
	
	     if (evt.getSource()==next) next();
	     if (evt.getSource()==back) back();
	     if (evt.getSource()==set) {
	       setDirectory();
	       location.setText("   Location: "+path);
         }
         if (evt.getSource()==rand) {
            //Graphics g=getGraphics();
            RandomSet rs = new RandomSet(imVector,path);
            rs.setSize(600,600);
            //= getGraphics();
            //rs.display();
            rs.show();
                    
         }   
            
         super.actionPerformed(evt);        
	     repaint();
	}
	

}

///////////////////////////////////////////////////////////////////////////////////////////

class RandomSet extends ClosableFrame {
       
       private    Vector    vector;
       private    int[]     select;
       private    String    path;
       private    Image[]     image;   
             
       public RandomSet(Vector vect,String aPath){
          path = aPath;
          vector = new Vector();
          vector = vect;
          image = new Image[9];
          randomize();
          setTitle("Thumbnail view");
          setBackground(Color.gray);     
       }
       
       public void paint(Graphics g) {
         
	     int x,y;
	     for(int i=0; i<9 ;i++){
	       x = 200*(i%3);
	       y = 200*(i - i%3)/3; 
           String fn = path+(String) vector.elementAt(select[i]); 
           image[i]=openImage(fn);
           g.drawImage(image[i],x,y,200,200,this);
      	 }     
       }
      
       public void randomize(){
         select = new int[9];
         int n,i=0;
         boolean duplicate=false;
         while(i<9) {         
           n=(int)( Math.random()*vector.size()) ;
           duplicate=false;
           for (int j=0;j<i;j++) 
            if (n==select[j]) duplicate=true;
           if (!duplicate) select[i++]=n; 
         }      
       }
           
	 
	 public Image openImage(String fn) {
	 
	     Image image =Toolkit.getDefaultToolkit().getImage(fn);
	     waitForImage(this, image);
	     return image;
	 }     
	 
	 public void  waitForImage(Component component,Image image) {
  
 	     MediaTracker tracker = new MediaTracker(component);        
 	     try {
          tracker.addImage(image, 0);
 	      tracker.waitForID(0);
	      if (!tracker.checkID(0)) 
	           System.out.println("Load failure!");
	     }
         catch(InterruptedException e) {  }
    }
} 