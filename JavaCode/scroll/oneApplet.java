import java.awt.*;
import java.applet.Applet;
import java.awt.event.*;

public class oneApplet extends Applet 
          {
         
          private Scrollbar slider;
          private int sliderValue=0;
          
          public void init() {
                    
                slider = new Scrollbar(Scrollbar.HORIZONTAL,0,1,0,100);
                add(slider);    
                slider.addAdjustmentListener(this);   
          }
          public void AdjustmentValueChanged(AdjustmentEvent evt) {
             
                sliderValue = slider.getValue();
                repaint();      
          }
          public void paint( Graphics g) {
                g.drawString("Current value is "+ sliderValue,100,100); 
          }
}      