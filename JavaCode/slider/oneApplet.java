/*
	Trivial applet that displays a string - 4/96 PNL
*/

import java.awt.*;
import java.applet.Applet;

public class oneApplet extends Applet
{
	private Scrollbar horzSlider;
        private int horz;

        public void init() {
                horzSlider=new Scrollbar(Scrollbar.HORIZONTAL,0,1,100);
                add(horzSlider);
		//repaint();
	}
	public void paint( Graphics g ) {
               rTriangle(g,100,100,150,80); 
	    
	}
	
	
            
	
	
	private void rTriangle(Graphics g,int x,int y,int horz,int vert) {
	
       	    float tangent=(float)vert/(float)horz;
	    g.drawString("gradient = "+tangent,30,120);
	    g.drawLine(x,y,x+horz,y);
	    g.drawLine(x+horz,y,x+horz,y-vert);
	    g.drawLine(x+horz,y-vert,x,y);
	}     
}
