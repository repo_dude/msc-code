/*
	Trivial applet that displays a string - 4/96 PNL
*/

import java.awt.*;
import java.applet.Applet;

public class TrivialApplet extends Applet
{
	public void init() {
		repaint();
	}
	public void paint( Graphics g ) {
	/*
	    // ex 4
		plotCircle(g,200,60,50);
	    
	    // ex 5
	    g.drawString(" 30*2 = "+ doubled(30),30,50);
	    // doubled(doubled(doubled(10))) should give ((2*10)*2)*2=80
	    int val=doubled(doubled(doubled(10)));
	    g.drawString("testing the result "+val,30,80);
	    
	    // ex 6
	  */  
	    rTriangle(g); 
	    
	}
	
	private void plotCircle (Graphics g,int xCentre,int yCentre, int radius) {
	   g.drawOval(xCentre-radius,yCentre-radius,2*radius,2*radius);
	}    

    private int doubled(int value){
    
        int  d=2*value;
        return d;
        
	}
	
	private void rTriangle(Graphics g) {
	    
	    Scrollbar horzSlider,vertSlider;
	    horzSlider=new Scrollbar(Scrollbar.VERTICAL, 0, 20, 0, 300);
        horzSlider.setLocation(150,50);
        add(horzSlider);
 	    vertSlider=new Scrollbar(Scrollbar.VERTICAL, 0, 20, 0, 300);
        vertSlider.setLocation(400,50);
        add(vertSlider);
       
 
	    /*float tangent=(float)vert/(float)horz;
	    g.drawString("gradient = "+tangent,30,120);
	    g.drawLine(x,y,x+horz,y);
	    g.drawLine(x+horz,y,x+horz,y-vert);
	    g.drawLine(x+horz,y-vert,x,y);*/
	}     
}
