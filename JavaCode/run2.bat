@echo off
rem *--------------------------------------------------*
rem batch file for compiling and starting java applets
rem                                  k. saadi  09/2000
rem *--------------------------------------------------*
@echo.
@echo Compiling...
javac oneApplet.java 
@echo.
@echo starting the applet... 
pause
AppletViewer oneApplet.html
:end
@echo Done.
@echo off