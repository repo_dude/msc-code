/////////////////////////////////////////////////////////////////////////////////////////
//    Simulating a simple computer.                                                    //
//                                                                          K. Saadi   //
/////////////////////////////////////////////////////////////////////////////////////////

import java.io.*;
import java.util.*; 

public class Sim
{
	private  short      pCode;
    private  short      pData;
    private  short      InBuf;
    private  short      OutBuf;
	static   short[]    Mem;
	private  Processor  pr;
    
	
	public Sim(){
		pr  = new Processor();
		Mem = new short[0x0200];
		pCode = 0x0080;
		pData = 0x0100;
		InBuf = 0x0009;
		OutBuf= 0x000A;
    }
		
	public static void main (String[] args){
		Sim s = new Sim();
		char choice;
		displayMenu();
        String add ;  short addr;
		String dat ;  short k;
		
		do{
		    System.out.print("\n>");
			choice = Line.readChar();
			choice = Character.toUpperCase(choice);
				    
			switch(choice){
				case 'V': System.out.print("address:");
				          add  = Line.get();
				          addr = Short.parseShort(add.substring(2,6),16);
				          System.out.print("size   :");
				          dat = Line.get();
				          k = Short.parseShort(dat.substring(2,6),16);
				          System.out.println("\n");
				          for (int i=0;i<k;i++)
				           System.out.println(tools.toHex(addr)+"  "+tools.toHex(Mem[addr++])); 
				         break;
				
				case 'W': s.construct();
				         break;
				
				case 'R': s.execute(false);
				         break;
				
				case 'T': s.execute(true);
				         break; 
				case 'X': s.loadProgram2();
		                  s.execute(false);
		                  s.checkProgram2();
				         break;
				case 'S': System.out.print("address:");
				          add  = Line.get();
				          add  = add.substring(2,6);
				          addr = Short.parseShort(add,16);
				          System.out.print("data   :");
				          dat = Line.get();
				          k = Short.parseShort(dat.substring(2,6),16);
				          Mem[addr]=k;
				        break;
				case '?': displayMenu();
				        break;
				default : if(choice!='Q') System.out.println("press ?");               
			}	
		}while(choice!='Q') ;
		
	}
	
	static void displayMenu(){
		System.out.println("==============================================");
		System.out.println("w : write an assembly code");
		System.out.println("x : run a program that already exists in memory");
		System.out.println("v : view memory contents " ); 
		System.out.println("r : run the code that has been already written");
		System.out.println("t : same as r with single tracing ");
		System.out.println("s : set memory location ");
		System.out.println("? or any other key : displays this menu ");
		System.out.println("q : quit");
		System.out.println("==============================================");
	}		  
	
	public void loadProgram1(){
		// loading to memory the simple example given in the assign(p5);
		// a = a + b
		Mem[0x0080]=(short)(0x0100); // movxa 0x0100
		Mem[0x0081]=(short)(0x2101); // addxa 0x0101
		Mem[0x0082]=(short)(0x100A); // movax 0x000A
		Mem[0x0083]=(short)(0x9000); // waout
		Mem[0x0084]=(short)(0x4000); // haltm
		
		Mem[0x0100]=400;// a
		Mem[0x0101]=200;// b
		
    }
    
    public void loadProgram2(){
    	
    	// moving an array from memory location 1 to memory location 2
    	// location 1 = 0x0100 , location 2 = 0x0180
    	// data
    	Mem[0x0100]=50; 
	    Mem[0x0101]=120;
	    Mem[0x0102]=25;
	    Mem[0x0103]=-36;
	    Mem[0x0104]=25;
	    Mem[0x0105]=40;
	    Mem[0x0106]=430;
	    Mem[0x0107]=8;
	    Mem[0x0108]=10;
	    Mem[0x0109]=-200;
        Mem[0x010A]=10; // size of the array;
        Mem[0x010B]=1;  // used for decrementation
        
        String sample="\n"+
        
        "Address    Hex code     Assembly         \n" +
        
        "0x0080     0x0500       MOVXA #1 0x0100  \n" + 
        "0x0081     0x1580       MOVAX #1 0x0180  \n" + 
        "0x0082     0xE400       IIJPX #1 0x0000  \n" +  
        "0x0083     0x010B       MOVXA    0x010B  \n" +
        "0x0084     0x3000       TWOCA            \n" + 
        "0x0085     0x210A       ADDXA    0x010A  \n" + 
        "0x0086     0x110A       MOVAX    0x010A  \n" + 
        "0x0087     0x6080       BIPTX    0x0080  \n" +  
        "0x0088     0x4000       HALTM             ";
        System.out.println("Simple program : transfering one block of memory \n"+sample);
        System.out.println("\npress any key");Line.get();
        System.out.println("\n Checking..");     
       
        // loading memory with instructions to be executed
        
        Mem[0x0080] = (short)0x0500;
        Mem[0x0081] = (short)0x1580;
        Mem[0x0082] = (short)0xE400;
        Mem[0x0083] = (short)0x010B;
        Mem[0x0084] = (short)0x3000;
        Mem[0x0085] = (short)0x210A;
        Mem[0x0086] = (short)0x110A;
        Mem[0x0087] = (short)0x6080;
        Mem[0x0088] = (short)0x4000;
    }
    
    public void checkProgram2(){
    	
    	System.out.println("Location 0x0100      Location 0x180\n");
    	for(int i=0;i<=9;i++)
    		System.out.println("\t"+Mem[0x0100+i]+"\t\t"+Mem[0x180+i]);
    }   
        
	public void execute(boolean trace){
		
		short opCode=0;
		short address;
		short IRnumber;
		short ind;
        
        pr.initialize();		
		pr.PC = pCode;
			
		
		while (opCode!=0x0004){	
			
			pr.IR = Mem[pr.PC];

			// extracting the opCode and address from the instruction word
			opCode  =(short)(0x000F&pr.IR>>12);
			address =(short)(0x01FF&pr.IR);
			IRnumber=(short)(0x0003&(pr.IR>>10));  // Index Register number :1,2,3.
			ind     =(short)(0x0001&(pr.IR>>9));   // indirect mode?
			
        	pr.PC++;// address of the next instruction word if not specified
			
			// Calculating the data address:
			if(ind==0)
				switch(IRnumber){
				   case 0 : pr.AR=address;                     break;
				   case 1 : pr.AR=(short)(address+pr.indexR1); break;
			       case 2 : pr.AR=(short)(address+pr.indexR2); break;
			       case 3 : pr.AR=(short)(address+pr.indexR3); break;
			    }
			if(ind==1)
				switch(IRnumber){
				   case 0 : pr.AR=Mem[address];                     break;
				   case 1 : pr.AR=(short)(Mem[address]+pr.indexR1); break;
			       case 2 : pr.AR=(short)(Mem[address]+pr.indexR2); break;
			       case 3 : pr.AR=(short)(Mem[address]+pr.indexR3); break;
			    }
			//** end **
			if(trace){
			   System.out.println(pr);
			   Line.get();
			}      
			switch (opCode){
				
				// MOVXA x
				case 0x0000: pr.DR = Mem[pr.AR];           
						   break;
				
				// MOVAX x
				case 0x0001: Mem[pr.AR]=pr.DR;             
						   break;
				
				// ADDXA x
				case 0x0002: pr.DR =(short)(pr.DR+Mem[pr.AR]);     
						   break;
				
				// TWOCA 
				case 0x0003: pr.DR=(short)(-pr.DR);                  
				             //pr.CCR=(short)(0x0001);
				      	   break;
				
				// HALTM
				case 0x0004:                                 
						     System.out.println("\n program ended! \n");
						   break;
				
				// BRUTX x
				case 0x0005: pr.PC = address;                 
						   break;
				
				// BIPTX x
				case 0x0006: if((0x8000&pr.DR)==0)          
				             pr.PC = address;
				           break;
				
				// BINTX x
				case 0x0007: if((0x8000&pr.DR)==0x8000)            
					         pr.PC = address;
						   break;
				
				// RINPA 
				case 0x0008: System.out.print("data :");
				             String inp = Line.get();
				             inp = inp.substring(2,6);
				             short value = Short.parseShort(inp,16);
				             Mem[InBuf]=value;                   
						   break;
				
				// WAOUT 
				case 0x0009: System.out.println(tools.toHex(Mem[OutBuf]));
						   break;
				
				// SHALF
				case 0x000A: pr.DR =(short)(pr.DR<<1);                            
						   break;
				
				// SHART
				case 0x000B: pr.DR =(short)(pr.DR>>1);            
						   break;
				
				// MOVXI  I x
				case 0x000C: if (IRnumber==1) pr.indexR1=Mem[address];  
					 	     if (IRnumber==2) pr.indexR2=Mem[address];
					         if (IRnumber==3) pr.indexR3=Mem[address];
					         if (IRnumber==0) System.out.println("Error");
					      
					       break;  
				
				// MOVIX  I x   
				case 0x000D: if (IRnumber==1) Mem[address]=pr.indexR1; 
					 	     if (IRnumber==2) Mem[address]=pr.indexR2;
					         if (IRnumber==3) Mem[address]=pr.indexR3;
					         if (IRnumber==0) System.out.println("Error");
		
					       break;  
				
				// IIJPX  I x 		  
  				case 0x000E: if (IRnumber==1)
  				              if(++pr.indexR1==0) pr.PC = address; 
  				            
  				    	     if (IRnumber==2) 
					 	      if(++pr.indexR2==0) pr.PC = address;
					      
					         if (IRnumber==3) 
					          if(++pr.indexR3==0) pr.PC = address;
			
					         if (IRnumber==0) System.out.println("Error");
					      
					      break;  
                
                //DIJPX  I x
  				case 0x000F: if (IRnumber==1)
  				              if(--pr.indexR1==0) pr.PC = address; 
  				    	    
  				    	     if (IRnumber==2) 
					 	      if(--pr.indexR2==0) pr.PC = address;
					      
					         if (IRnumber==3) 
					          if(--pr.indexR3==0) pr.PC = address;
		
		                     if (IRnumber==0) System.out.println("Error");
						  break;
			}// end switch
			
		}//end while
	}
	
	public void construct(){

		String reading;
		String text="";
		
		String code=""; 
		String rn="";
		String add="";
		
		String next="";
		
		boolean ind =false ;
		short   indR=0 ;
		short   addr=0 ;
		short   opCode=0;
		
		StringTokenizer st;
		
		short programCode=pCode;
		short instructionWord=0;	
		
		while((code.compareTo("HALTM"))!=0){
			System.out.print(tools.toHex(programCode)+"  ");// dispaying the instr address
			reading=Line.get();
			//text+=reading;
			st = new StringTokenizer(reading," ");
			
			code=""; 
		    rn="0";
		    add="0";
			ind  =false;
		    indR =0;
		    addr =0;
		    instructionWord=0;

			if (st.hasMoreTokens()) next = st.nextToken();
			code = next.toUpperCase(); // extracting the operande code
			if (st.hasMoreTokens()) next = st.nextToken();
			if (next.charAt(0)=='#') { // indexed mode #i:indexRi
					rn   = next.substring(1,2);
					indR = Short.parseShort(rn,10);
					next = st.nextToken();
			}
			if (next.charAt(0)=='('){  // (0x4567) indicates indirect mode
					ind = true; 
				    add = next.substring(3,7);
			}
			if (next.charAt(0)=='0') add = next.substring(2,6);
			addr = Short.parseShort(add,16);
    	    
    	    if(ind) instructionWord = (short)(0x0001<<9);
    	    
    	    instructionWord = (short)((indR<<10)|instructionWord|addr);
		    
		    if (code.compareTo("MOVXA")==0) opCode=(short)0x0000;
		    if (code.compareTo("MOVAX")==0) opCode=(short)0x1000;
   			if (code.compareTo("ADDXA")==0) opCode=(short)0x2000;
		    if (code.compareTo("TWOCA")==0) opCode=(short)0x3000;
		    if (code.compareTo("HALTM")==0) opCode=(short)0x4000;
		    if (code.compareTo("BRUTX")==0) opCode=(short)0x5000;
		    if (code.compareTo("BIPTX")==0) opCode=(short)0x6000;
		    if (code.compareTo("BINTX")==0) opCode=(short)0x7000;
		    if (code.compareTo("RINPA")==0) opCode=(short)0x8000;
 			if (code.compareTo("WAOUT")==0) opCode=(short)0x9000;
            if (code.compareTo("SHALF")==0) opCode=(short)0xA000;
            if (code.compareTo("SHART")==0) opCode=(short)0xB000;
            if (code.compareTo("MOVXI")==0) opCode=(short)0xC000;
            if (code.compareTo("MOVIX")==0) opCode=(short)0xD000;
            if (code.compareTo("IIJPX")==0) opCode=(short)0xE000;
            if (code.compareTo("DIJPX")==0) opCode=(short)0xF000;
			
			instructionWord  = (short)(opCode|instructionWord);
			Mem[programCode] = instructionWord;
			// back to text
			add="0x"+add;
			if(ind) add="("+add+")";
			if(code.compareTo("HALTM")==0) add="      ";  
			if(code.compareTo("TWOCA")==0) add="      ";  
			if(code.compareTo("SHALF")==0) add="      ";  
			if(code.compareTo("SHART")==0) add="      ";  
			if(code.compareTo("RINPA")==0) add="      ";  
			if(code.compareTo("WAOUT")==0) add="      ";  
			text+=tools.toHex(programCode)+"      "+tools.toHex(Mem[programCode])+"\t\t"+
			      code+" "+ (indR==0?"  ":"#"+Short.toString(indR))+"  "+add+"\n";
			
			programCode++;
		}
		System.out.println("\nAddress     Hex code            Assembly code");
		System.out.println("\n"+text);
	}		
}

class Processor{
	
   public  short   PC;      // Program Counter
   public  short   IR;      // Instruction Register
   public  short   AR;      // address Register
   public  short   DR;      // Data Register
   public  short   indexR1; // Index Register 1
   public  short   indexR2; // Index Register 2
   public  short   indexR3; // Index Register 3
   public  short   CCR;     // Cond. Code Register
   
   public Processor(){
        initialize(); 
   }
   
   public void initialize(){
   	   	PC=0; IR=0; AR=0; DR=0;
   		indexR1=0; indexR2=0; indexR3=0;
   		CCR=0;
   }		

   public String toString(){
   	    String content= "PC="+tools.toHex(PC)+" IR="+tools.toHex(IR)
   	       +" AR="+tools.toHex(AR)+" DR="+tools.toHex(DR)
   	       +" indexR1="+tools.toHex(indexR1)+" indexR2="+tools.toHex(indexR2)
   		   +" indexR3="+tools.toHex(indexR3)+" CCR="+tools.toHex(CCR);	
   	    return content;
   }	 	   
} 

class Line {		 
   // reading a string from the dos prompt
   static DataInputStream entered= new DataInputStream(System.in);
   //reading a line
   public static String get(){
      String temp;
      try {
          temp = entered.readLine();
      } catch( Exception e ) {
          e.printStackTrace();
          temp = null;
          System.exit(-1);
      }
      return temp;
   }
   // reading a character
   public static char readChar(){
      String temp="";
      char res=' ';
      try {
        temp = entered.readLine();
        if (temp.length()==0) res='u';
        else res = temp.charAt(0);
      } catch( Exception e ) {
          e.printStackTrace();
          res = '?';
          System.exit(-1);
      }
      return res;
   }
}

class tools{
	// converting a short to an Hexadecimal string
	public static String toHex(short x){
		short[] digit=new short[4];
		digit[3]=(short)((x>>12)&0x000F);
		digit[2]=(short)((x>>8)&0x000F);
		digit[1]=(short)((x>>4)&0x000F);
		digit[0]=(short)(x&0x000F);
		String res="0x";
		for(int i=3;i>=0;i--){
		 switch(digit[i]){
		  case 0  : res+="0";break;
		  case 1  : res+="1";break;
		  case 2  : res+="2";break;
		  case 3  : res+="3";break;
		  case 4  : res+="4";break;
		  case 5  : res+="5";break;
		  case 6  : res+="6";break;
		  case 7  : res+="7";break;
		  case 8  : res+="8";break;
		  case 9  : res+="9";break;
		  case 10 : res+="A";break;
		  case 11 : res+="B";break;
		  case 12 : res+="C";break;
		  case 13 : res+="D";break;
		  case 14 : res+="E";break;
		  case 15 : res+="F";break;
		 }  
        } 
        return res;
   }
}

