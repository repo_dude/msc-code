/*
	Trivial applet that displays a string - 4/96 PNL
*/

import java.awt.*;
import java.applet.Applet;
import java.awt.event.*;

public class oneApplet extends Applet implements ActionListener 
{
	private TextField  accountField;
	private Label      accountLabel;
	private Account    james;
	private int        amount=0;
	
	public void init() {
	    
	    james = new Account();
	    accountLabel = new Label(" Amount : o");
	    accountField = new TextField(10);
	    
	    add(accountLabel);
	    add(accountField);
	    accountField.addActionListener(this);
		//repaint();
	}
	
	public void actionPerformed(ActionEvent event) {
	
	       amount = Integer.parseInt(accountField.getText());
	       repaint();
	}       
	    
	public void paint( Graphics g ) {
		
		james.updateBalance(amount);
		g.drawString( "Balance  : o"+james.getBalance(), 20, 60 );
		accountField.setText("");
		if (james.overdraft()) {
		   g.setColor(Color.red);
		   g.drawString( "o"+Math.abs(james.getBalance())+" Overdrawn", 20, 80 );
		}   
		
	}
}


class  Account {

       private int balance=0;
       
       public void Account() {
            //balance=newBalance; 
       }
       public int getBalance() {
            return balance;
       }
       
       public void updateBalance(int newAmount) {
            balance=balance+newAmount;
       }     
       
       public boolean overdraft() {
            if (balance<0) return true;
            else return false;
       }
}              
            


       
