@echo off
rem *--------------------------------------------------*
rem batch file for compiling and starting java applets
rem                                  k. saadi  09/2000
rem *--------------------------------------------------*
cls
@echo.
@echo Compiling...
javac oneApplet.java 
@echo.
pause
@echo starting the applet... 
AppletViewer oneApplet.html
:end
@echo Done.
@echo off