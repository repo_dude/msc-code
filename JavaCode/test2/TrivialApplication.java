import gui.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import java.awt.*;
//import java.util.*;
import java.io.*;
import java.util.zip.*;
//import java.awt.event.*;
//import java.awt.image.*;
//import java.net.*;


public class Fun extends ImageFrame{

   private   boolean         done;   
   private  int[]            pexs;
   private  int[][]          hist;
   private  ClosableFrame  f= new ClosableFrame();
   private  ImageSequence seq; 
   //public   String[] list= new String[]={"1001","1002","1003","1004","1005","1006","1007"
   //                         "1008","1009","1010"}; 

   Menu nextMenu = new Menu("option");
   MenuItem next = addMenuItem(nextMenu,"next..");
   
   
   public Fun(String name){
       super(name);
       MenuBar menuBar= getMenuBar();
       menuBar.add(nextMenu);
       setMenuBar(menuBar);
       //seq = new ImageSequence();
       //seq.open();
   }    
   
   
   public void actionPerformed(ActionEvent evt){
      String pathName="C:\\windows\\desktop\\sample\\";
      done=false;
      if (match(evt, next)) {
         done=true;
         //setImageResize(seq.next());
         //openGif(seq.next());
         return;
      }
      super.actionPerformed(evt);
   }      
   
   
   public static void main(String args[]) {
        
        //ImageFrame gf = new ImageFrame("Image Frame");
        Fun gf = new Fun("Image Frame");
        gf.setSize(200,200);
        gf.show();
        
        
         
        /*gf.f.setSize(200,200);
        gf.ImageToPels();
        gf.stat();
        gf.display();
        gf.f.show();*/
         
   }
   
   public void ImageToPels() {
                 
        pexs = new int[width * height];
	    
	    ColorModel cm = ColorModel.getRGBdefault();
        
	    PixelGrabber grabber = 	new PixelGrabber(
				                        image, 0, 0, 
                           				width, height, pexs, 0, width);

   }
   
   private int index=0;
   public void stat() {
        int value;
        int count;
        
        boolean exists;
        hist = new int[2][width*height];
        
        for(int i=0;i<pexs.length;i++){
             value=pexs[i];
             
             exists=false;
             for(int k=0;k<=index;k++){
               if (value==hist[0][k]) exists=true;
             }  
             if (!exists) {
               count=0;
               for(int j=i ;j<=pexs.length;j++) {
                 if (value==pexs[j]) count++;        
               }
               hist[0][index]=value;
               hist[1][index]=count;
               index++;
             }
        }
   }             
        
        
   public void display() {
   
      Graphics g = f.getGraphics();
      for (int i=0; i<index; i++) 
         g.drawRect(100+5*i,100+5*i,5,hist[1][i]/100); 
   }
   
   public void paint(Graphics g) {
   
   if (done) {
      g.drawImage(seq.next(),0,0,300,300,this);
    } 
   }      
      
}


class ImageSequence {
  
  private int index =0;
  private int width = 0;
  private int height = 0;
  private Vector imageVector = 
  	new Vector();
	


public int getSize() {
	return imageVector.size();
}
public void setWidth(int w) {
	width = w;
}
public void setHeight(int h) {
	height = h;
} 
public int getWidth() {
	return width ;
}
public int getHeight() {
	return height ;
}

public void setIndex(int i) {
	index = i;
}
public void add(Image img) {
	imageVector.addElement(img);
}

public Image pels2Image(
	int pels[],int w, int h) {
 Toolkit tk = Toolkit.getDefaultToolkit();
 return tk.createImage(
				new MemoryImageSource(
					w, 
					h,
					ColorModel.getRGBdefault(), 
					pels, 0, 
					w));
}
public int [] toPels(Image img) {
   	int pels[] = new int[width * height];
	PixelGrabber grabber = 
			new PixelGrabber(
				img, 0, 0, 
				width, 
				height, 
				pels, 0, 
				width);
	try {grabber.grabPixels();}
	catch (InterruptedException e){};
	return pels;	
}
public void open(){
  open(
	ImageFrame.getReadFileName());
}

public Image next() {
	Image img =(Image)
	imageVector.elementAt(index++);
	if (index == 
		imageVector.size()-1) 
			index =0;
	return img;
}

public Image elementAt(int i) {
	return (Image)
		imageVector.elementAt(i);
}

public int [][] toPels() {
	int n = getSize();
	int pels[][] = new int[n][];
	for (int i=0; i < n; i++) 
		pels[i]=toPels(i);
	return pels;
}

public void resetImages() {
	imageVector = new Vector();
	index = 0; 
}

public int [] toPels(int i) {
	return
		toPels(elementAt(i));
}

public void save(String fn) {
	try {
    	FileOutputStream fos = 
    	new FileOutputStream(fn);
        ZipOutputStream gos = 
          new ZipOutputStream(fos);
     	ObjectOutputStream oos = 
     	  new ObjectOutputStream(gos);
     	save(oos);
        oos.close();
        gos.finish();
        gos.close();

        } catch(IOException e) {
        	System.out.println(e);
        }
        System.out.println("done");
	}

public void save(
   ObjectOutputStream oos) 
	throws IOException {
	oos.writeInt(width);
	oos.writeInt(height);
	System.out.println(width);
	System.out.println(height);
	oos.writeInt(getSize());
	for (int i=0; i < getSize(); i++)
		oos.writeObject(toPels(i));
}

public void open(InputStream 
         is) 
	throws IOException, ClassNotFoundException {
        ZipInputStream gis = 
        new ZipInputStream(is);
     	ObjectInputStream ois 
     	  = new ObjectInputStream(gis);        
        open(ois);
        ois.close();
}

public void open(String fn) {
	imageVector = new Vector();
    try {
    	FileInputStream fis = new FileInputStream(fn);
        ZipInputStream gis = new ZipInputStream(fis);
     	ObjectInputStream ois = new ObjectInputStream(gis);        
        open(ois);
        ois.close();
        //gis.finish();

        } catch(Exception e) {
        	System.out.println(e);
        }
     System.out.println("done reading images");

}

public void open(
 ObjectInputStream ois) 
	throws IOException, 
		ClassNotFoundException {

    width = ois.readInt();
    height = ois.readInt();
    int numberOfImages = ois.readInt();
    for (int i=0; 
    	i < numberOfImages; 
    	i++)
   		add(
   			pels2Image(
   				(int[])
   				ois.readObject(),
   				width,height));
}

/*public static void main(String[] args) {
   ImageSequence next = new ImageSequence();
   next.open();
   Syste
}*/

}








